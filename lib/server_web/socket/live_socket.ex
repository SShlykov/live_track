defmodule ServerWeb.LiveSocket do
  @moduledoc """
  The LiveView socket for Phoenix Endpoints.
  """
  use Phoenix.Socket

  defstruct id: nil,
            endpoint: nil,
            view: nil,
            parent_pid: nil,
            root_pid: nil,
            router: nil,
            assigns: %{__changed__: %{}},
            private: %{__changed__: %{}},
            fingerprints: Phoenix.LiveView.Diff.new_fingerprints(),
            redirected: nil,
            host_uri: nil,
            transport_pid: nil

  defoverridable init: 1

  @impl true
  def init(state) do
    res = {:ok, {_, socket}} = super(state)
    on_connect(self(), socket)
    res
  end

  channel "lvu:*", Phoenix.LiveView.UploadChannel
  channel "lv:*", Phoenix.LiveView.Channel

  @impl Phoenix.Socket
  def connect(_params, %Phoenix.Socket{} = socket, connect_info) do
    {:ok, put_in(socket.private[:connect_info], connect_info)}
  end

  @impl Phoenix.Socket
  def id(socket), do: socket.private.connect_info[:session]["live_socket_id"]

  defp on_connect(pid, %Phoenix.Socket{private: %{connect_info: %{session: session}}} = socket) do
    LiveMonitor.Niflheim.update_user_session(
      LiveMonitor.Niflheim.get_user_session(session["session_id"]),
      %{socket_connected: true, socket_pid: pid_to_string(pid)}
    )

    monitor(pid, socket, session)
    :ok
  end

  defp on_connect(_pid, _socket), do: Logger.warn("cant connect")

  defp on_disconnect(_socket, session) do
    case session["session_id"] && LiveMonitor.Niflheim.get_user_session(session["session_id"]) do
      %{id: _} = session ->
        LiveMonitor.Niflheim.update_user_session(session, %{
          socket_connected: false,
          finished_dt: NaiveDateTime.local_now()
        })

      _nil ->
        :ok
    end

    case session["session_id"] &&
           LiveMonitor.Niflheim.get_unfinished_user_session_statistic(session["session_id"]) do
      sessions when is_list(sessions) ->
        Enum.each(
          sessions,
          &LiveMonitor.Niflheim.update_user_session_statistic(&1, %{
            finished_dt: NaiveDateTime.local_now()
          })
        )

      _nil ->
        :ok
    end

    case session["device"] && LiveMonitor.Niflheim.get_user_device_ip(session["device"]) do
      %{id: _} = device -> LiveMonitor.Niflheim.update_user_device_ip(device, %{is_active: false})
      _nil -> :ok
    end

    :ok
  end

  defp monitor(pid, socket, session) do
    Task.Supervisor.start_child(LiveMonitor.UserSessionsSupervisor, fn ->
      Process.flag(:trap_exit, true)
      ref = Process.monitor(pid)

      receive do
        {:DOWN, ^ref, :process, _pid, _reason} ->
          on_disconnect(socket, session)
      end
    end)
  end

  def pid_to_string(pid) when is_pid(pid) do
    pid
    |> inspect(limit: :infinity)
    |> String.replace(~s(#PID<), "")
    |> String.replace(~s(>), "")
  end
end
