defmodule ServerWeb.Router do
  use ServerWeb, :router
  import ServerWeb.UserAuth
  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ServerWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :browser_outer do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ServerWeb.LayoutView, :outer}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ServerWeb do
    pipe_through [:browser_outer, :redirect_if_user_is_authenticated]
    live "/hello_world", HelloWorld
    get "/users/login", UserSessionController, :new
    post "/users/login", UserSessionController, :create
  end

  scope "/", ServerWeb do
    pipe_through [:browser, :require_authenticated_user]

    live "/", PageLive
  end

  scope "/", ServerWeb do
    pipe_through :browser

    get "/users/logout", UserSessionController, :delete
    live_dashboard "/dashboard", metrics: ServerWeb.Telemetry
  end
end
