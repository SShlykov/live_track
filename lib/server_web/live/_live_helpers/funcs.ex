defmodule ServerWeb.HelperFuncs do
  @moduledoc false
  import Phoenix.LiveView
  use Phoenix.Component

  def get_user_from_session(%{"user_token" => token}),
    do: Server.Accounts.get_user_by_session_token(token)

  def get_user_from_session(_), do: nil

  def connect_to_topic(socket, topic) do
    if connected?(socket) do
      Phoenix.PubSub.subscribe(Server.PubSub, topic)
    end

    socket
  end

  def assign_menu(socket, session) do
    user = get_user_from_session(session)

    socket
    |> connect_to_topic("common")
    |> assign(:menu_list, menu())
    |> assign(:current_user, user)
  end

  def update_menu(socket) do
    socket
    |> assign(:menu_list, menu())
  end

  def menu do
    List.flatten([
      [
        %{type: :simple, show: "Главная", icon: "home-2-line", link: "/"},
        %{type: :line}
      ]
    ])
  end

  def remix_icon(assigns) do
    assigns =
      assigns
      |> assign_new(:class, fn -> "" end)
      |> assign(:attrs, assigns_to_attributes(assigns, [:icon, :class]))

    ~H"""
    <i class={"ri-#{@icon} #{@class}"} {@attrs}></i>
    """
  end

  def broadcast(topic, message), do: Phoenix.PubSub.broadcast(Server.PubSub, topic, message)
end
