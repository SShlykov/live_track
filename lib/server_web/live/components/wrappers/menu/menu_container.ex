defmodule ServerWeb.Component.MenuContainer do
  @moduledoc false
  use ServerWeb, :live_component
  alias ServerWeb.Component.Menu

  def render(%{current_menu_item: current_menu_item} = assigns)
      when is_binary(current_menu_item) do
    assigns = Map.put(assigns, :current_user, assigns.current_user || %{})

    site_name = Application.get_env(:server, :site_name)

    ~H"""
    <div class="flex" x-data="{showMenu: false}">
      <div class="flex-grow">
        <%= live_component Menu, id: :main_menu, current_menu_item: @current_menu_item, current_user: @current_user, menu_list: @menu_list %>
        <div class="bg-white px-4 py-2 shadow-md border-gray-200 fixed w-full" style="z-index: 6;">
          <div class="flex justify-between items-center">
          <div class="flex gap-x-6 items-center">
              <div @click="showMenu = !showMenu" :class="showMenu ? 'burger_btn active' : 'burger_btn'">
                <div></div>
                <div></div>
                <div></div>
              </div>
              <span class="bg-gray-500" style="height: 15px; width: 1px"></span>
              <span  class="flex items-center">
                <svg style="height: 30px; width: 30px" width="52" height="62" viewBox="0 0 52 62" fill="#4A91F2" xmlns="http://www.w3.org/2000/svg">
                  <path d="M0.202099 39.5362C0.0892499 45.7138 1.44344 47.1178 1.44344 47.1178C27.0038 49.4765 32.872 49.5888 41.7871 47.6794C35.2983 46.1069 24.2955 48.5779 0.202099 39.5362Z" fill="#4A91F2"/>
                  <path d="M0.371389 22.4638L0 34.538C14.9854 42.288 44.4627 46.2192 44.4627 46.2192C34.0569 57.5072 33.8877 54.3062 30.5022 58.5743C30.5022 59.8659 40.9172 56.3279 52 43.4112C2.17698 32.7971 4.20827 26.0018 0.371389 22.4638Z" fill="#4A91F2"/>
                  <path d="M10.4714 0C12.2206 12.4112 11.487 20.5543 43.1977 35.2681C47.3732 36.6159 24.8033 61.8877 18.4837 62C18.258 62 18.9915 60.9464 19.1044 60.8207C26.447 52.6399 32.2513 46.7246 35.0726 37.6268C8.8351 27.6304 -6.68169 20.1051 10.4714 0Z" fill="#4A91F2"/>
                </svg>
                <span><%= site_name %></span>
              </span>
            </div>
            <%= link to: "/users/logout", method: :get, class: "logout p-4 cursor-pointer flex mobile-menu-button items-center gap-x-2" do %>
              <span><%= "#{Map.get(@current_user, :first_name)} #{Map.get(@current_user, :last_name)}" %></span>
              <button class="outline-none mobile-menu-button flex items-center justify-center">
                <svg
                  class="w-4 h-4 text-gray-800"
                  fill="#6B7280"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  viewBox="0 0 384.971 384.971"
                  stroke="currentColor"
                >
                  <g>
                    <g id="Sign_Out">
                      <path d="M180.455,360.91H24.061V24.061h156.394c6.641,0,12.03-5.39,12.03-12.03s-5.39-12.03-12.03-12.03H12.03    C5.39,0.001,0,5.39,0,12.031V372.94c0,6.641,5.39,12.03,12.03,12.03h168.424c6.641,0,12.03-5.39,12.03-12.03    C192.485,366.299,187.095,360.91,180.455,360.91z"/>
                      <path d="M381.481,184.088l-83.009-84.2c-4.704-4.752-12.319-4.74-17.011,0c-4.704,4.74-4.704,12.439,0,17.179l62.558,63.46H96.279    c-6.641,0-12.03,5.438-12.03,12.151c0,6.713,5.39,12.151,12.03,12.151h247.74l-62.558,63.46c-4.704,4.752-4.704,12.439,0,17.179    c4.704,4.752,12.319,4.752,17.011,0l82.997-84.2C386.113,196.588,386.161,188.756,381.481,184.088z"/>
                    </g>
                  </g>
                </svg>
              </button>
            <% end %>
          </div>
          <div class="sm:hidden flex items-center justify-between">
            <a class="hw-font cursor-pointer text-center w-full" style="font-size: 2rem" href="/"><%= site_name %></a>
          </div>
        </div>
        <div class="flex flex-col py-4 container-block overflow-y-auto" style="margin-top: calc(55px + 1rem); max-height: calc(100vh - 55px - 1rem);">
          <%= @inner_block.(%{}, assigns) %>
        </div>
      </div>
    </div>
    """
  end
end
