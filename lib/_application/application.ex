defmodule Server.Application do
  @moduledoc false

  use Application
  @cluster_name :scandinavian
  @where_is_global (System.get_env("GLOBAL_ADDR") || "global@172.25.78.153")
                   |> String.downcase()
                   |> String.to_atom()

  @impl true
  def start(_type, _args) do
    children =
      List.flatten([
        {Phoenix.PubSub, [name: Server.PubSub, adapter: Phoenix.PubSub.PG2]},
        ServerWeb.Telemetry,
        ServerWeb.Endpoint,
        # Server.Repo,
        {Registry, keys: :unique, name: LiveTrack.SimpleRegistry},
        {Task.Supervisor, name: LiveTrack.TaskSupervisor, max_children: Application.get_env(:server, :max_machines)},
        Supervisor.child_spec(
          {Task, fn -> LiveTrack.Receiver.init(Application.get_env(:server, :port)) end},
          id: :tcp_receiver_receiver,
          restart: :permanent
        ),
        Supervisor.child_spec(
          {Task, fn -> LiveTrack.Receiver.init(5162) end},
          id: :tcp_receiver_egts,
          restart: :permanent
        ),
        Supervisor.child_spec(
          {Task, fn -> LiveTrack.ReceiverScout.init(20654) end},
          id: :tcp_receiver_scout,
          restart: :permanent
        ),
        LiveTrack.Supervisor,
        prod_children(Mix.env()),
        external_api(Node.self())
      ])

    opts = [strategy: :one_for_one, name: Server.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def external_api(_) do
    []
  end

  def prod_children(:prod) do
    gnode = [global_node_addr: @where_is_global]

    topologies = [
      scandinavian: [
        strategy: Cluster.Strategy.Epmd,
        config: [hosts: [@where_is_global]]
      ]
    ]

    [
      {Cluster.Supervisor, [topologies, [name: @cluster_name]]},
      {Server.Niflheim, gnode},
      {Server.Emailer, gnode},
      {Server.GStore, gnode},
      {Server.SqlSup, gnode},
      {Server.GlobalGuardian.Server, {:global, make_name(Application.get_env(:server, :server_name))}}
    ]
  end
  def prod_children(_), do: []

  @impl true
  def config_change(changed, _new, removed) do
    ServerWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp make_name(default) do
    "SERVER_NAME"
    |> System.get_env()
    |> Kernel.||(default)
    |> String.downcase()
    |> String.to_atom()
  end
end
