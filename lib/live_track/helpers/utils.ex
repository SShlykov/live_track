defmodule LiveTrack.Helpers.Utils do
  @moduledoc """
  Вспомогательные функции
  """
  require Logger
  def send_response(socket, {:ok, data}), do: send_response(socket, data)

  def send_response(socket, data) do
    msg = Poison.encode!(%{byte_size: bz(data)})
    send_message(socket, :simple, msg)

    socket
  end

  def bz(str) do
    str
    |> byte_size
  end

  def read_response(line) do
    line
    |> String.trim()
    |> Jason.decode!()
  rescue
    _e -> false
  end

  @doc """
  Записывает данные в файл и оповещает всех пользователей подписанных на обновление машины
  """
  def write_file(%{auth: auth, data_strings: data_strings}) do
    # %{end_sign: _end_sign, imei: imei, key: key, num: num, pream: _pream} =
    #   TcpReceiver.Parcer.parce_pack(auth)

    # device = "7_#{imei}_#{Enum.join(:binary.bin_to_list(key), ",")}"

    # dt_start = NaiveDateTime.local_now() |> Timex.format!("%Y-%m-%d", :strftime)
    # file_name = "#{File.cwd!()}/priv/machine_data/[#{dt_start}]/#{device}.txt"
    # dt = NaiveDateTime.local_now() |> Timex.format!("%Y-%m-%d %H:%M:%S", :strftime)

    # TcpReceiver.HelperFuncs.broadcast("devices", {"new_records", device, length(data_strings)})

    # if !File.exists?(file_name) do
    #   File.mkdir("#{File.cwd!()}/priv/machine_data/[#{dt_start}]")
    #   File.touch!(file_name)
    # end

    Enum.map(data_strings, fn data ->
      data = :binary.bin_to_list(data) |> Enum.join(",")
      # TcpReceiver.HelperFuncs.broadcast("device:#{device}", {"push_new", "[#{dt}]", "#{data}"})
      # File.write!(file_name, "[#{dt}]: #{data}\n", [:append])
    end)
  end

  def take_safely(list, func) do
    case list do
      [] -> []
      list when is_list(list) -> func.(list)
    end
  end

  def slice_data(data_string) when is_binary(data_string) do
    data_string
    |> String.trim()
    |> String.split(",")
    |> Enum.map(&String.trim/1)
    |> Enum.filter(& &1)
  end

  def slice_data(any), do: any

  def is_message(message, msg) when is_binary(message),
    do:
      message
      |> String.replace("\n", "")
      |> String.replace("\r", "")
      |> String.downcase()
      |> Kernel.==(String.downcase(msg))

  def is_message({:ok, message}, msg),
    do: message |> String.replace("\n", "") |> String.replace("\r", "") |> Kernel.==(msg)

  def is_message(_, _), do: false

  def add_if(str, arr) when is_list(arr) do
    arr ++ [str]
  end

  def add_if(str, _arr) do
    [str]
  end

  @doc """
  считывает строку, если доступна

      iex> LiveTrack.Helpers.Utils.read_line!(socket)
      data
      iex> LiveTrack.Helpers.Utils.read_line!(closed_socket)
      ** (throw) "could not be recognized"

  """
  def read_line!(socket) do
    case read_line(socket) do
      {:ok, data} -> data
      :closed -> throw("could not be recognized")
    end
  end

  @doc """
  считывает строку, если доступна

      iex> LiveTrack.Helpers.Utils.read_line!(socket)
      {:ok, data}
      iex> LiveTrack.Helpers.Utils.read_line!(closed_socket)
      :closed

  """
  @spec read_line(any()) :: :closed | {:ok, any}
  def read_line(socket) do
    case :gen_tcp.recv(socket, 0, :timer.minutes(15)) do
      {:ok, data} -> {:ok, data}
      {:error, :closed} -> :closed
      critical -> error_close(critical)
    end
  end

  @doc """
  Устройства на данный момент не принимают ответных сообщений
  """
  def send_message(socket, :zip, _message) do
    # resp = Poison.encode!(%{message: inspect(message)}) |> :zlib.gzip()
    # send_message(socket, :simple, resp)
    socket
  end

  def send_message(socket, :mapify, _message) do
    # resp = Poison.encode!(%{message: inspect(message)})
    # send_message(socket, :simple, resp)
    socket
  end

  def send_message(socket, :simple, _message) do
    # :gen_tcp.send(socket, message <> "\n")

    socket
  end

  def error_close(msg) do
    Logger.debug(msg, application: :tcp_receiver)
    :closed
  end

  def auth_to_service_name(auth), do: auth |> inspect() |> String.to_atom()
end
