defmodule LiveTrack.Validator do
  @moduledoc false
  require Logger

  @doc """
  Сатус валидации

      iex> TcpReceiver.Validators.validation_status("123321,421412,412,3357427843\n", :simple)
  """
  def set_protocol_buffer(line) do
    cond do
      is_wialon(line) ->
        LiveTrack.Structures.WialonBuffer.new(line)

      is_niac(line) ->
        LiveTrack.Structures.NiacBuffer.new(line)

      true ->
        {:error, :unknown_buffer_type}
    end
  end

  def is_wialon(line), do: LiveTrack.Structures.WialonBuffer.is_valid_login(line)
  def is_niac(line), do: LiveTrack.Structures.NiacBuffer.is_valid_login(line)

  # def validation_status(line, :simple) do
  #   sliced_data = TcpReceiver.Helpers.slice_data(line)

  #   cond do
  #     TcpReceiver.Helpers.is_message(line, "0") ->
  #       :close_message

  #     sliced_data |> length |> Kernel.>=(4) ->
  #       check_summs(sliced_data)

  #     true ->
  #       :invalid
  #   end
  # end

  # def check_summs(list) do
  #   [summ | pack] = list |> :lists.reverse()
  #   summ = String.to_integer(summ)
  #   pack_summ = pack |> :lists.reverse() |> Enum.join(",") |> :erlang.crc16()

  #   if(summ == pack_summ,
  #     do: {:valid, :sum_eq, Enum.join(list, ",")},
  #     else: {:valid, :pack_broken}
  #   )
  # rescue
  #   _ -> :invalid
  # end
end
