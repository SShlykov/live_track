defmodule LiveTrack.Supervisor do
  @moduledoc """
  Супервизор управляющий автоматом прямой приемки данных (простейший режим)
  Создает дочерний процесс (автомат) под каждое утройство и свзывает процесс в реестре
  Также позволяет извне управлять каждым узлом (запускать, остановливать, просматривать)
  """
  use DynamicSupervisor
  @supervisor_name :tcp_server
  alias LiveTrack.SimpleRegistry

  def create(socket, %{auth: %{body: %{imei: imei}}} = buffer) do
    with {:ok, pid} <- find_or_create(imei),
         {:ok, :running} <- call(imei, {:run, socket, buffer}) do
      {:connected, pid}
    end
  end

  def create(_, _) do
    {:error, "cant connect"}
  end

  @spec stop(String.t()) :: :ok | {:error, :not_exist | :not_found}
  def stop(auth) do
    case find(auth) do
      {:ok, pid} ->
        DynamicSupervisor.terminate_child(@supervisor_name, pid)

      e ->
        e
    end
  end

  def call(name, params) do
    case find(name) do
      {:ok, pid} -> GenServer.call(pid, params, :infinity)
      {:error, :not_exist} -> {:error, :not_exist}
    end
  end

  def list_workers do
    SimpleRegistry
    |> Registry.select([{{:"$1", :"$2", :"$3"}, [], [{{:"$1", :"$2", :"$3"}}]}])
    |> Enum.map(&elem(&1, 0))
  end

  def start_link(_opts) do
    DynamicSupervisor.start_link(__MODULE__, [], name: @supervisor_name)
  end

  @impl true
  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one, extra_arguments: [])
  end

  def stop_supervisor(timeout \\ 5000) do
    Supervisor.stop(__MODULE__, :normal, timeout)
  end

  defp start(name) do
    name = {:via, Registry, {SimpleRegistry, name}}

    DynamicSupervisor.start_child(@supervisor_name, {LiveTrack.Worker, name})
  end

  defp find_or_create(auth) do
    case Registry.lookup(SimpleRegistry, auth) do
      [] -> {:ok, _pid} = start(auth)
      [{pid, nil}] -> {:ok, pid}
    end
  end

  defp find(auth) do
    case Registry.lookup(SimpleRegistry, auth) do
      [] -> {:error, :not_exist}
      [{pid, nil}] -> {:ok, pid}
    end
  end
end
