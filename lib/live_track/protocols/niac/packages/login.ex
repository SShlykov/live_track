defmodule Niac.Packages.Login do
  defstruct ptype: "L",
            prot_version: nil,
            body: nil,
            log: nil,
            respond: nil

  def new(["2", imei, key | _], _opts) do
    # TODO: crc проверка
    %__MODULE__{prot_version: "1", body: body(imei, key)}
    |> put_log()
  end

  def body(imei, key), do: %{imei: imei, key: key}

  defp put_log(mod), do: %__MODULE__{mod | log: %{state: :connection, message: inspect(mod)}}
end
