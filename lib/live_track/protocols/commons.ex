defmodule LiveTrack.Packages.Helpers do
  def put_log(%{opts: opts} = mod, %{log: log}, proto_version) do
    if Keyword.get(opts, :log) do
      %{mod | log: Map.put(log, :version, proto_version)}
    else
      %{mod | log: nil}
    end
  end

  def put_log(mod, _, _), do: %{mod | log: nil}

  def put_respond(%{opts: opts} = mod, %{respond: respond}) do
    if Keyword.get(opts, :respond) do
      %{mod | respond: respond}
    else
      %{mod | respond: nil}
    end
  end

  def put_respond(mod, _, _), do: %{mod | respond: nil}
end
