defmodule LiveTrack.Structures.ScoutBuffer do
  use Bitwise
  alias Scout.Packages
  alias Scout.Packages.{DataPackage, DataPackagesCount}
  alias LiveTrack.Packages.Helpers

  defstruct version: nil,
            type: __MODULE__,
            messages: [],
            packs_counter: 0,
            opts: nil,
            auth: nil,
            log: nil,
            respond: nil,
            finish: false

  def pack_types do
    [
      PC: %{
        type: "Packages Count",
        structure: DataPackagesCount
      },
      P: %{
        type: "Package",
        structure: DataPackage
      },
    ]
  end

  def new(line, opts \\ []) do
    opts = Keyword.merge([log: true, respond: true, validate: false], opts)
    {type, parsed_message} = read_pack(line, 0)

    protocol_version = 1
    case read_message(parsed_message, 1, type, 0) do
      {:ok, %DataPackagesCount{finish: finish, body: packs_counter} = parsed_message} ->
        res =
          %__MODULE__{
            version: protocol_version,
            packs_counter: packs_counter,
            finish: finish,
            opts: opts
          }
          |> Helpers.put_log(parsed_message, protocol_version)
          |> Helpers.put_respond(parsed_message)

        {:ok, res}

      msg ->
        {:error, msg}
    end
  rescue
    e ->
      IO.inspect(e)
      {:error, "ошибка при создании буфера"}
  end

  def add(%__MODULE__{version: protocol_version, messages: _messages, packs_counter: packs_counter} = mod, line) do
    {type, parsed_message} = read_pack(line, packs_counter)

    case read_message(parsed_message, 1, type, packs_counter) do
      {:ok, %DataPackagesCount{body: packs_counter} = parsed_message} ->
        %__MODULE__{mod | packs_counter: packs_counter}
        |> Helpers.put_log(parsed_message, protocol_version)
        |> Helpers.put_respond(parsed_message)

      {:ok, %DataPackage{body: _body, rem_messages: _rem_messages, packs_counter: packs_counter} = parsed_message} ->
        # %__MODULE__{mod | messages: body ++ messages, packs_counter: packs_counter}
        %__MODULE__{mod | packs_counter: packs_counter}
        |> Helpers.put_log(parsed_message, protocol_version)
        |> Helpers.put_respond(parsed_message)
    end
  end

  def read_pack(line, packs_counter) do
    type = case packs_counter do
      0 -> :PC
      _ -> :P
    end
    {type, line |> String.replace("\r", "") |> String.replace("\n", "") |> Packages.chunk_bits(16)}
  end

  def read_message(msg, protocol_version, type, packs_counter) do
    valid = is_valid_type(type)

    cond do
      !valid ->
        {:error, msg}

      is_number(protocol_version) ->
        %{structure: struct} = Keyword.get(pack_types(), type)
        options = %{type: type, packs_counter: packs_counter}
        parsed_message = apply(struct, :new, [msg, options])

        {:ok, parsed_message}

      true ->
        {:error, msg}
    end
  end

  def finalize(%__MODULE__{messages: nil}, socket) do
    :gen_tcp.close(socket)
  end

  def finalize(%__MODULE__{messages: messages}, socket) do
    :gen_tcp.close(socket)
    LiveTrack.Logs.do_log(%{log_type: "warn", state: "has_messages", message: inspect(messages, limit: :infinity)})
  end

  def is_valid_login(line) do
    line = line |> String.replace("\r", "") |> String.replace("\n", "")
    case line do
      <<_::binary-size(8)>> -> true
      _ -> false
    end
  end
  # def is_valid_login(<<_::binary-size(8)>>), do: true
  # def is_valid_login(_), do: false

  defp is_valid_type(type), do: type in Keyword.keys(pack_types())

  # def is_valid(line) do
  #   ["", type, _raw_message] = String.split(line, "#")

  #   is_valid_type(type)
  # rescue
  #   _e -> false
  # end
end
