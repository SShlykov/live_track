defmodule Scout.Packages do
  def chunk_bits(string, size) do
    for <<chunk::size(size) <- string>>, do: <<chunk::size(size)>>
  end

  def put_respond(mod) do
    Map.put(mod, :respond, "55")
  end

  def invalid_imei?(_imei), do: false
  def invalid_time?(%{date: date, time: time}), do: !((!is_nil(date) && String.length(date) == 6) && (!is_nil(time) && String.length(time) == 6))
  def invalid_coordintes?(%{lat: lat, lon: lon}) do
    (lat == :error || lon == :error || lat >= 90 || lat <= -90 || lon >= 180 || lon <= -180 )
  end
  def invalid_speed?(%{speed: speed}), do: ((speed == :error) || (speed < 0))
  def invalid_sattelites?(%{sats: sats}), do: ((sats == :error) || (sats < 0))
  def invalid_io?(%{inputs: inputs, outputs: outputs}), do: ((inputs == :error) || (outputs == :error))
  def invalid_adc?(map) do
    Enum.reduce_while(map, false, fn {k, v}, _acc ->
      if String.match?("#{k}", ~r/adc/) && (v == :error) do
        {:halt, true}
      else
        {:cont, false}
      end
    end)
  end
  def invalid_additionals?(map) do
    Enum.reduce_while(map, false, fn {k, v}, _acc ->
      if String.match?("#{k}", ~r/adc/) && (v == :error) do
        {:halt, true}
      else
        {:cont, false}
      end
    end)
  end

  def put_log(mod), do: %{mod | log: %{log_type: "info", state: "server", message: inspect(mod.body)}}
  # def finalize(%{status: "1"} = mod), do: mod
  # def finalize(mod), do: %{mod | finish: true}
end

# defimpl Inspect, for: Wialon.Packages.Login do
#   import Inspect.Algebra

#   def inspect(%Wialon.Packages.Login{} = package, opts) do
#     list =
#       for attr <- [:status, :body] do
#         {attr, Map.get(package, attr)}
#       end

#     container_doc("#Login<", list, ">", opts, fn
#       {:status, status}, opts -> concat("status: ", to_doc(status, opts))
#       {:body, body}, opts -> concat("body: ", to_doc(body, opts))
#     end)
#   end
# end

# defimpl Inspect, for: Wialon.Packages.DataPackage do
#   import Inspect.Algebra

#   def inspect(%Wialon.Packages.DataPackage{ptype: ptype} = package, opts) do
#     list =
#       for attr <- [:status, :body] do
#         {attr, Map.get(package, attr)}
#       end

#     container_doc("##{ptype}<", list, ">", opts, fn
#       {:status, status}, opts -> concat("status: ", to_doc(status, opts))
#       {:body, body}, opts -> concat("body: ", to_doc(body, opts))
#     end)
#   end
# end

# defimpl Inspect, for: Wialon.Packages.BlackBox do
#   import Inspect.Algebra

#   def inspect(%Wialon.Packages.BlackBox{} = package, opts) do
#     list =
#       for attr <- [:status, :body] do
#         {attr, Map.get(package, attr)}
#       end

#     container_doc("#BlackBox<", list, ">", opts, fn
#       {:status, status}, opts -> concat("status: ", to_doc(status, opts))
#       {:body, body}, opts -> concat("body: ", to_doc(body, opts))
#     end)
#   end
# end

# defimpl Inspect, for: Wialon.Packages.Ping do
#   import Inspect.Algebra

#   def inspect(%Wialon.Packages.Ping{}, opts) do
#     container_doc("#Pong<", [], ">", opts, fn
#       {:status, status}, opts -> concat("status: ", to_doc(status, opts))
#       {:body, body}, opts -> concat("body: ", to_doc(body, opts))
#     end)
#   end
# end
