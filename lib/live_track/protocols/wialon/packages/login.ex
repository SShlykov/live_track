defmodule Wialon.Packages.Login do
  alias Wialon.Packages
  defstruct ptype: "L",
            prot_version: nil,
            body: nil,
            status: nil,
            log: nil,
            respond: nil,
            finish: false

  def new([prot_version, imei, password | _] = msg, %{respond: respond}) do
    %__MODULE__{prot_version: prot_version, body: body(imei, password), status: status(msg)}
    |> Packages.put_respond(respond)
    |> Packages.put_log()
    |> Packages.finalize()
  end

  def new([_imei, _password | _] = msg, %{respond: respond}), do: new(["1.1" | msg], %{respond: respond})

  def body(imei, password), do: %{imei: imei, password: password}

  def status([_prot_version, imei, password | _] = msg) do
    cond do
      ((length(msg) == 4) && Packages.invalid_crc?(msg)) -> "10"
      Packages.invalid_password?(password) -> "01"
      Packages.invalid_imei?(imei) -> "0"
      true -> "1"
    end
  end
end
