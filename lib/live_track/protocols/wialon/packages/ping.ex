defmodule Wialon.Packages.Ping do
  defstruct ptype: nil,
            log: nil,
            respond: nil

  def new(_msg, %{respond: respond}) do
    %__MODULE__{}
    |> put_respond(respond)
    |> put_log()
  end

  defp put_log(mod), do: %__MODULE__{mod | log: %{state: :ping, message: "ping", log_type: "info"}}

  def put_respond(mod, respond) do
    {resp, _} = Code.eval_string(respond, Map.to_list(mod))
    Map.put(mod, :respond, resp)
  rescue
    _e ->
      Map.put(mod, :respond, "#AP#\r\n")
  end
end
