defmodule Wialon.Packages.DataPackage do
  import ShorterMaps
  alias Wialon.Packages
  defstruct ptype: nil,
            body: nil,
            respond: nil,
            status: nil,
            log: nil,
            line: nil,
            finish: nil

  def new(msg, %{fields: fields, type: type, respond: respond}) do
    package = body(msg, fields)

    %__MODULE__{ptype: type, body: package, line: msg, status: status(type, package, msg, fields)}
    |> Packages.put_respond(respond)
    |> Packages.put_log()
    |> Packages.finalize()
  end

  def body(msg, fields) do
    Enum.zip(fields, msg)
    |> Enum.map(fn
      {k, "NA"} -> {k, nil}
      {:date, date} -> {:date, date}
      {:time, time} -> {:time, time}
      {:unzip_params, ""} -> []
      {:unzip_params, params} -> {:params, unzip_params(params)}
      {:lat1, lat1} -> {:lat1, lat1}
      {:lat2, lat2} -> {:lat2, lat2}
      {:lon1, lon1} -> {:lon1, lon1}
      {:lon2, lon2} -> {:lon2, lon2}
      {:adc, ""} -> []
      {:adc, params} -> unzip_adc(params)
      {:lbutton, params} -> {:lbutton, params}
      {k, v} -> {k, try_num(v)}
    end)
    |> List.flatten()
    |> Enum.reject(& &1 == [])
    |> Enum.into(%{})
    |> reparse()
    |> try_put_pack_dt
  end

  defp try_int(val) do
    {val, ""} = Integer.parse(val)
    val
  rescue
    _e -> :error
  end

  defp try_float(val) do
    {val, ""} = Float.parse(val)
    val
  rescue
    _e -> :error
  end
  defp try_num(val) do
    try_val = fn v, func ->
      case func.(v) do
        :error -> nil
        v -> v
      end
    end

    try_val.(val, &try_int/1) || try_val.(val, &try_float/1)
  end

  defp reparse(map) do
    ~M{lat1, lat2, lon1, lon2} = map

    map
    |> Map.put(:dt, NaiveDateTime.local_now())
    |> Map.put(:lat, latify(lat1, lat2))
    |> Map.put(:lon, longify(lon1, lon2))
  rescue
    _E -> map
  end

  def latify(nil, _), do: nil
  def latify(lat, "S") do
    case latify(lat, "N") do
      :error -> :error
      lat -> -lat
    end
  end
  def latify(lat, "N") do
    int = String.to_float(lat) |> Kernel.*(:math.pow(10, 4)) |> ceil()
    grad = div(int, 1000000)
    min = rem(int, 1000000) / (600000)
    grad + Float.round(min, 6)
  rescue
    _e -> :error
  end
  def latify(_, _), do: :error

  def longify(nil, _), do: nil
  def longify(lon, "W") do
    case longify(lon, "E") do
      :error -> :error
      lon -> -lon
    end
  end
  def longify(lon, "E") do
    int = String.to_float(lon) |> Kernel.*(:math.pow(10, 4)) |> ceil()
    grad = div(int, 1000000)
    min = rem(int, 1000000) / (600000)
    grad + Float.round(min, 6)
  rescue
    _e -> :error
  end
  def longify(_, _), do: :error

  defp try_put_pack_dt(%{date: date, time: time} = pack) do
    {:ok, dt} = Timex.parse("#{date}T#{time}", "{YY}{M}{D}T{h24}{m}{s}")
    Map.put(pack, :pack_dt, dt)
  rescue
    _e ->
      Map.put(pack, :pack_dt, :error)
  end

  def unzip_adc({param, index}) do
    {:"adc_#{index}", try_num(param)}
  rescue
    _e -> {:"error_#{index}", {:cant_unzip_adc, param}}
  end

  def unzip_adc(params) when is_binary(params) do
    params
    |> String.split(",", trim: true)
    |> Enum.with_index(1)
    |> Enum.reject(& elem(&1, 0) == "")
    |> Enum.map(&unzip_adc/1)
  rescue
    _e -> {:params, params}
  end

  def unzip_param({param, index}) do
    [string, type, value] = String.split(param, ":", trim: true)

    value =
      cond do
        type == "1" -> String.to_integer(value)
        type == "2" -> String.to_float(value)
        true -> value
      end

    {:"#{string}", value}
  rescue
    _e ->
      {:"error_#{index}", {:cant_unzip, param}}
  end

  def unzip_params(params) when is_binary(params) do
    params
    |> String.split(",", trim: true)
    |> Enum.with_index()
    |> Enum.map(&unzip_param/1)
  rescue
    _e -> {:params, params}
  end

  def status("Short Data", package, msg, fields) do
    len_d = abs(length(msg) - length(fields))
    cond do
      (len_d > 1) -> "-1"
      Packages.invalid_time?(package) -> "0"
      Packages.invalid_coordintes?(package) -> "10"
      Packages.invalid_speed?(package) -> "11"
      Packages.invalid_sattelites?(package) -> "12"
      (len_d == 1) && Packages.invalid_crc?(msg) -> "13"
      true -> "1"
    end
  end

  def status("Extended Data", package, msg, fields) do
    len_d = abs(length(msg) - length(fields))
    cond do
      (len_d > 1) -> "-1"
      Packages.invalid_time?(package) -> "0"
      Packages.invalid_coordintes?(package) -> "10"
      Packages.invalid_speed?(package) -> "11"
      Packages.invalid_sattelites?(package) -> "12"
      Packages.invalid_io?(package) -> "13"
      Packages.invalid_adc?(package) -> "14"
      Packages.invalid_additionals?(package) -> "15"
      (len_d == 1) && Packages.invalid_crc?(msg) -> "16"
      true -> "1"
    end
  end
end
