defmodule Helpers.DynamicStructDestroyer do
  def destroy(map) when is_map(map), do: :maps.map(&do_destroy/2, map)
  def destroy(list) when is_list(list), do: Enum.map(list, &(destroy(&1) |> repack))

  def do_destroy(_key, value), do: ensure_nested_map(value)

  defp ensure_nested_map(list) when is_list(list), do: Enum.map(list, &ensure_nested_map/1)
  defp ensure_nested_map(%{__cardinality__: _}), do: nil

  defp ensure_nested_map(%{__struct__: _, calendar: _} = struct), do: struct

  defp ensure_nested_map(%{__struct__: _} = struct),
    do: :maps.map(&do_destroy/2, from_struct(struct))

  defp ensure_nested_map(data), do: data

  defp repack(map) when is_struct(map), do: from_struct(map)

  defp repack(map) when is_map(map) do
    Enum.to_list(map)
    |> Enum.map(fn
      {_, nil} -> nil
      {:children, []} -> nil
      {:columns, []} -> nil
      {_, %{__cardinality__: _}} -> nil
      {k, v} -> {k, v}
    end)
    |> Enum.filter(& &1)
    |> Enum.into(%{})
  end

  defp from_struct(struct) do
    waste_fields = [:__meta__]

    struct
    |> Map.from_struct()
    |> Map.drop(waste_fields)
    |> repack
  end
end
