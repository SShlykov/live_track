defmodule Helpers.ReportBuilder do
  @moduledoc false
  import Ecto.Query, warn: false

  def group_routes_by(grouped_by, query) do
    classifications = grouped_by[:classifications] || []
    railways = grouped_by[:railways] || []
    orgs = grouped_by[:orgs] || []
    machines = grouped_by[:machines] || []
    # summary         = grouped_by[:summary]         || []

    query = (railways != [] && from(gq in query, where: gq.railway_id in ^railways)) || query

    query =
      (classifications != [] &&
         from(gq in query, where: gq.classification_parent_id in ^classifications)) || query

    query = (orgs != [] && from(gq in query, where: gq.org_id in ^orgs)) || query
    (machines != [] && from(gq in query, where: gq.machine_id in ^machines)) || query
  end

  def query_reducer(args, query, date_field \\ :dates_dt, grouper_func) do
    Enum.reduce(args, query, fn
      {:filters, list_of_filters}, query ->
        Enum.reduce(list_of_filters, query, fn %{filter_by: filter_by, filter_case: filter_case} =
                                                 _filter,
                                               query ->
          filter_by = Recase.to_snake(filter_by) |> String.to_atom()

          cond do
            filter_by in ~w(classification_name classification_parent_name type_name drp_name drp_name org_name type first_lin)a ->
              from(u in query, where: ilike(field(u, ^filter_by), ^"%#{filter_case}%"))

            filter_by in ~w(machine_id classification_id classification_parent_id drp org_id zav_nomer last_lin rw_id type_id dates)a ->
              from(u in query, where: field(u, ^filter_by) in ^filter_case)

            true ->
              query
          end
        end)

      {:order, order}, query ->
        from u in query, order_by: [{^order, :end_dt}]

      {:limit, limit}, query ->
        from u in query, limit: ^limit

      {:group_by, grouped_by}, query ->
        grouper_func.(grouped_by, query)

      {:machine, machine_id}, query ->
        case machine_id do
          nil ->
            query

          id ->
            from u in query,
              where: u.machine_id == ^id
        end

      {:available_machines, available_machines}, query ->
        case available_machines do
          nil ->
            from u in query,
              where: u.machine_id in []

          list ->
            from u in query,
              where: u.machine_id in ^list
        end

      {:dates, dates}, query ->
        start = dates.start |> Timex.parse!("{YYYY}-{0M}-{D}") |> Timex.to_date()

        finish =
          dates.finish
          |> Date.from_iso8601!()
          |> Date.add(1)
          |> Date.to_string()
          |> Timex.parse!("{YYYY}-{0M}-{D}")
          |> Timex.to_date()

        from u in query,
          where: field(u, ^date_field) >= ^start and field(u, ^date_field) < ^finish

      _, query ->
        query
    end)
  end
end
