defmodule Mix.ConstVals do
  def init(%Mix.File{path: path, context: context, context_name: context_name} = file) do
    filename = get_filename(path)
    file_type = get_file_type(path)

    %Mix.File{file | data: file_data(file_type, filename, context, context_name)}
  end

  defp file_data(".ex", _, "", context_name) do
    web_module = web_module()

    """
    defmodule #{web_module}.#{context_name} do
      @moduledoc false

      use #{web_module}, :live_view
      alias #{web_module}.Component.MenuContainer

      def mount(_, session, socket) do
        socket =
          socket
          |> assign_menu(session)
          |> assign(:current_menu_item, "#{context_name}")

        {:ok, socket}
      end

      def render(assigns) do
        ~H\"\"\"
        <%= live_component MenuContainer, id: :main_menu_component, current_menu_item: @current_menu_item, current_user: @current_user, menu_list: @menu_list do %>
          <div  class="flex flex-col px-4"  style="width: 100vw">
            <%= live_react_component("Components.#{context_name}", [], id: "#{Phoenix.Naming.underscore(context_name)}_id") %>
          </div>
        <% end %>
        \"\"\"
      end

      def handle_params(_, _, socket) do
        socket =
          socket
          |> assign(:params, [])

        {:noreply, socket}
      end

      def handle_event("init_component", _search_panel_params, socket) do
        {:noreply, socket}
      end
    end
    """
  end

  defp file_data(".jsx", "/index.jsx", "funcs", _context_name) do
    """
    export const parseSepareteDate = (
      date = new Date(),
      symbol = ".",
      mode = "DateDMY"
    ) => {
      const selfDate = getDateByType(date);
      let dateBits = {
        year: selfDate.getFullYear(),
        month: selfDate.getMonth() + 1,
        day: selfDate.getDate(),
        hours: selfDate.getHours(),
        minutes: selfDate.getMinutes(),
        seconds: selfDate.getSeconds(),
      };
      Object.keys(dateBits).forEach((key) => {
        // @ts-ignore
        if (dateBits[key] < 10) dateBits[key] = "0" + dateBits[key];
      });
      switch (mode) {
        case "DateDMY":
          return dateBits.day + symbol + dateBits.month + symbol + dateBits.year;
        case "DateDM":
          return dateBits.day + symbol + dateBits.month;
        case "DateYMD":
          return dateBits.year + symbol + dateBits.month + symbol + dateBits.day;
        case "DateDMYHM":
          return (
            dateBits.day +
            symbol +
            dateBits.month +
            symbol +
            dateBits.year +
            " " +
            dateBits.hours +
            ":" +
            dateBits.minutes
          );
        case "DateDMYHMS":
          return (
            dateBits.day +
            symbol +
            dateBits.month +
            symbol +
            dateBits.year +
            " " +
            dateBits.hours +
            ":" +
            dateBits.minutes +
            ":" +
            dateBits.seconds
          );
        case "DateYMDHM":
          return (
            dateBits.year +
            symbol +
            dateBits.month +
            symbol +
            dateBits.day +
            " " +
            dateBits.hours +
            ":" +
            dateBits.minutes
          );
        case "DateHM":
          return dateBits.hours + ":" + dateBits.minutes;
        case "DateDataBase":
          return `${
            dateBits.year + symbol + dateBits.month + symbol + dateBits.day
          }T${dateBits.hours}:${dateBits.minutes}:${dateBits.seconds}`;
        case "DateHMDM":
          return (
            dateBits.hours +
            ":" +
            dateBits.minutes +
            " " +
            dateBits.day +
            symbol +
            dateBits.month
          );
        default:
          return dateBits.day + symbol + dateBits.month + symbol + dateBits.year;
      }
    };
    """
  end

  defp file_data(".jsx", "/index.jsx", "styles", _context_name) do
    """
    import styled from "styled-components";

    export const Wrapper = styled.div`
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      row-gap: 20px;
      column-gap: 20px;
      flex-wrap: wrap;
    `;
    """
  end

  defp file_data(".jsx", "/index.jsx", "", context_name) do
    """
    import React from 'react';
    import { parseSepareteDate } from "./funcs";
    import { Wrapper } from "./styles";
    import { withContext } from "../../wrappers";

    const defaultComponentSettings = { selected_date: new Date() };
    const defaultComponentData     = { report: [] };
    const defaultLayoutSettings    = { mode: "standart", loading: {init: true} };

    const defaultContextValue = {
      componentSettings: defaultComponentSettings,
      componentData: defaultComponentData,
      layoutSettings: defaultLayoutSettings
    };
    const ComponentContext = React.createContext({ context: defaultContextValue });

    const #{context_name} = withContext(ComponentContext, defaultContextValue, (props) => {
      const context = React.useContext(ComponentContext);
      const { componentDataState, componentSettingsState, layoutSettingsState } = context.states;
      const [componentData, setComponentData, cComponentData] = componentDataState;
      const [componentSettings, setComponentSettings, cComponentSettings] = componentSettingsState;
      const [layoutSettings, setLayoutSettings, cLayoutSettings, cByFieldLayoutSettings] = layoutSettingsState;

      const cLoading = (val) => cByFieldLayoutSettings("loading", "init", !!val)

      React.useEffect(() => {
        if (props?.handleEvent) {
          props.handleEvent("set_loading", ({ mode }) => cByFieldLayoutSettings("loading", mode, true));
        }
      })

      return (
        <Wrapper>
          Текущее время: {parseSepareteDate(componentSettings.selected_date, "-", "DateHM")}
        </Wrapper>
      )
    })

    export default #{context_name};
    """
  end

  defp file_data(".jsx", filename, context, _) do
    "// /#{context}/#{filename}"
  end

  defp file_data(".js", filename, context, _) do
    "// /#{context}/#{filename}"
  end

  defp file_data(".ex", filename, context, _) do
    "# /#{context}/#{filename}"
  end

  defp get_file_type(path) do
    case Regex.run(~r/\.[[:alnum:]]+$/, path) do
      [file_type] -> file_type
      nil -> nil
    end
  end

  defp get_filename(path) do
    case Regex.run(~r/\/[[:alnum:]]+\.[[:alnum:]]+$/, path) do
      [filename] -> filename
      nil -> nil
    end
  end

  defp web_module do
    base = Mix.Phoenix.base()

    cond do
      Mix.Phoenix.context_app() != Mix.Phoenix.otp_app() ->
        Module.concat([base])

      String.ends_with?(base, "Web") ->
        Module.concat([base])

      true ->
        Module.concat(["#{base}Web"])
    end
  end
end
