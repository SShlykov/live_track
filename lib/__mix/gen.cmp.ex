defmodule Mix.Tasks.Gen.Cmp do
  @moduledoc """
  Pulls from all repositories
  """
  # import HelperMix
  use Mix.Task
  import HelperMix
  alias Mix.ReactContext

  @avaliable_options ~w(-n)

  def run(opts) do
    options = read_options(opts, @avaliable_options)

    with ["-n", component_name] <-
           Enum.find(options, :no_component_name, fn [hd | _] -> hd == "-n" end),
         %ReactContext{} = ctx <- ReactContext.new(component_name, folders: to_create()),
         false <- ReactContext.pre_existing?(ctx) do
      build_files(ctx)

      Mix.shell().info("""

      Компонент создан: #{inspect(ctx.dir, pretty: true)}
      для использования в проекте импортируйте его:
          import {#{ctx.name}} from "./components"
      и поместите в объект window.Components
      """)
    else
      :no_component_name -> info("Отсутствует имя контекста")
      _ -> info("Контекст уже существует")
    end
  end

  def info(text) do
    Mix.shell().info("""
    Задача gen.component ожидает передачу обязательного флага -n
    таким образом необходимо вызвать её по следующему принципу:

        mix gen.cmp -n MyComponent

    Пожалуйста, убедитесь, что все параметры переданы правильно и
    данный контекст не существует
    """)

    Mix.shell().error("""
    Ошибка: #{text}
    """)
  end

  def to_create() do
    List.flatten([
      %{path: "tests", files: ["index.test.js"]},
      ~w(components styles funcs)
    ])
  end

  def build_files(context) do
    Mix.shell().info("""
    Приступаю к созданию файлов компонента #{context.name}
    """)

    # 1 - создать директорию
    make_directory(context.dir)
    # 2 - для files создать директорию и сами файлы
    make_files(context)
    # 3 - создать index файл
    make_files(context.base_file)
    # 4 - добавить к списку компонент
    add_to_components(context)
    :ok
  end

  defp add_to_components(%Mix.ReactContext{name: name}) do
    path = "/assets/js/components/index.js"
    lines = from_cwd(path) |> File.read!() |> String.split("\n")

    make_file(%{
      path: path,
      data: Enum.join([~s|export { default as #{name} } from "./#{name}";| | lines], "\n")
    })
  end

  defp make_directory(dir) do
    path = from_cwd(dir)
    File.mkdir_p!(path)
    Mix.shell().info("  Создана директория: #{dir}")
  end

  defp make_files(%Mix.ReactContext{files: files}) when is_list(files),
    do: Enum.map(files, &make_files/1)

  defp make_files(%Mix.Files{files: files, file_path: file_path}) do
    make_directory(file_path)
    Enum.each(files, &make_file/1)
  end

  defp make_file(%{path: path, data: data}) do
    dir = from_cwd(path)
    :ok = File.write!(dir, data)
    Mix.shell().info("    Создан документ: #{path}")
  end

  defp from_cwd(path), do: Path.join(File.cwd!(), path)
end
