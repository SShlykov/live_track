defmodule Mix.ReactContext do
  alias Mix.Files

  defstruct name: nil,
            base_module: nil,
            context_app: nil,
            opts: nil,
            dir: nil,
            base_file: nil,
            files: nil

  def new(context_name, opts) do
    ctx_app = opts[:context_app] || Mix.Phoenix.context_app()
    base = Module.concat([Mix.Phoenix.context_base(ctx_app)])

    if opts[:ex_files] do
      dir = "/lib/#{Atom.to_string(ctx_app)}_web/live/pages/"

      base_file =
        Mix.Files.new(
          %{path: "", files: ["#{Phoenix.Naming.underscore(context_name)}.ex"]},
          dir,
          context_name
        )

      %__MODULE__{
        name: context_name,
        base_module: base,
        context_app: ctx_app,
        opts: opts,
        dir: dir,
        base_file: base_file
      }
    else
      dir = "/assets/js/components/#{context_name}"
      files = Enum.map(opts[:folders], &Mix.Files.new(&1, dir, context_name))
      base_file = Mix.Files.new("", dir, context_name)

      %__MODULE__{
        name: context_name,
        base_module: base,
        context_app: ctx_app,
        opts: opts,
        dir: dir,
        base_file: base_file,
        files: files
      }
    end
  end

  def pre_existing?(%__MODULE__{base_file: %Files{files: files}}),
    do: Enum.reduce_while(files, false, &is_exists?/2)

  defp is_exists?(%Mix.File{path: path}, _) do
    if File.exists?(Path.join(File.cwd!(), path)) do
      {:halt, true}
    else
      {:cont, false}
    end
  end
end
