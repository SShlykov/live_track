defmodule Server.GlobalGuardian.SyncCondition do
  @moduledoc false
  require Logger

  def request(other) do
    Logger.warn(
      {:error,
       %{reason: "no-condition", params: other, service: :server_server, request_type: "handler"}},
      application: :server_server
    )

    {:error, "no-condition"}
  end

  def super_condition(param, state), do: {:reply, request(param), state}
end
