import Config

config :server, Server.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "172.25.78.36",
  database: "live_track_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

config :server, ServerWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "yje1oQ3JNWJIpWba7mmGOhSr6qViPcqewsg6TeZf9Upo0uNVz79USKApeKkpvcQA",
  server: false

config :server, Server.Mailer, adapter: Swoosh.Adapters.Test

config :server,
  server_name: "test_live_track",
  default_page_size: 100,
  max_page_size: 500,
  site_name: "Test site",
  max_machines: 100,
  port: 9091,
  wialon_password: ["valid_password", "NA"]

config :logger, level: :warn
config :phoenix, :plug_init_mode, :runtime
