# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :server,
  ecto_repos: [Server.Repo]

# Configures the endpoint
config :server, ServerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "u4G1FcP+d0SYQ2j2mn0D33NlLyhiecbFz4xs+p3pE3gFRNj6Ld9/Vo8MBxH2rPSA",
  render_errors: [view: ServerWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Server.PubSub,
  live_view: [signing_salt: "dG90b3RpdGl0dXR1dG91dG91"]

config :server, Server.Mailer, adapter: Swoosh.Adapters.Local

config :server, Server.Guardian,
  issuer: "cdrp",
  secret_key: "n5U4Fl78MjdSqCnpRq/GWUy6VlmQsC74TR5CoqkTRHdLilJtttDijJmgGh/zGbFg"

config :server,
  server_name: "Site Template",
  default_page_size: 100,
  max_page_size: 500,
  site_name: "Test site",
  max_machines: 100,
  port: 5039,
  # string || list
  wialon_password: ["valid_password", "NA"]

config :swoosh, :api_client, false

config :esbuild,
  version: "0.14.0",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2016 --define:global=window --outdir=../priv/static/assets --external:/images/* --external:/fonts/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason
import_config "#{config_env()}.exs"
