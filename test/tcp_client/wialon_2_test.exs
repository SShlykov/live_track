defmodule Test.Wialon2Test do
  use ExUnit.Case, async: false
  import Mock, warn: false

  @tcp_attrs [:binary, packet: :line, active: false, reuseaddr: true]

  setup do
    {:ok, _} = Application.ensure_all_started(:server)
    # Node.start(:"test_tcp@172.25.78.153")
    # Node.set_cookie(:"8FWrDX6nB9zCreE5")
    # Node.connect(:"global@172.25.78.153")
    port = Application.get_env(:server, :port) || 9876
      # port = 9090
    host = "127.0.0.1" |> String.to_charlist()
    # Тесты работают паралельно, поэтому к imei нужно добавлять число с номером теста
    imei = 862531045294676

    %{port: port, host: host, auth: %{imei: imei, password: "NA"}}
  end


  describe "wialon v2.0 // [пакет авторизации]:" do
    test "валидный imei и пароль", %{auth: %{imei: imei, password: password}} = sock_info do
      socket = connect(sock_info)
      body = "2.0;#{imei}20;#{password};"
      crc = LiveTrack.Crc16.calc(body)
      {:ok, reply} = send_and_fetch(socket, "#L##{body}#{crc}\r\n")
      :gen_tcp.close(socket)
      assert reply == "#AL#1\r\n"
    end

    test "невалидный пароль", %{auth: %{imei: imei}} = sock_info do
      socket = connect(sock_info)
      body = "2.0;#{imei}21;invalid_passwd;"
      crc = LiveTrack.Crc16.calc(body)
      {:ok, reply} = send_and_fetch(socket, "#L##{body}#{crc}\r\n")
      :gen_tcp.close(socket)
      assert reply == "#AL#01\r\n"
    end

    test "невалидный crc", %{auth: %{imei: imei}} = sock_info do
      socket = connect(sock_info)
      body = "2.0;#{imei}22;invalid_passwd;"
      crc = "123"
      {:ok, reply} = send_and_fetch(socket, "#L##{body}#{crc}\r\n")
      :gen_tcp.close(socket)
      assert reply == "#AL#10\r\n"
    end
  end

  describe "wialon v2.0 // [сокращенный пакет]:" do
    test "валидный пакет", sock_info do
      reply = do_send(sock_info, "#SD##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;")
      assert reply == "#ASD#1\r\n"
    end

    test "не валидная дата/время", sock_info do
      reply = do_send(sock_info, "#SD#0;#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;")
      assert reply == "#ASD#0\r\n"
    end

    test "не валидные координаты", sock_info do
      reply = do_send(sock_info, "#SD##{curr_date()};#{curr_time()};55044.6025;N;03739.6834;E;10;56;100;13;")
      assert reply == "#ASD#10\r\n"
    end

    test "не валидная скорость", sock_info do
      reply = do_send(sock_info, "#SD##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;-10;56;100;13;")
      assert reply == "#ASD#11\r\n"
    end

    test "не валидное количество спутников", sock_info do
      reply = do_send(sock_info, "#SD##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;-13;")
      assert reply == "#ASD#12\r\n"
    end

    test "не валидная структура (добавлены лишние параметры)", sock_info do
      reply = do_send(sock_info, "#SD##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;758;321;")
      assert reply == "#ASD#-1\r\n"
    end
  end

  describe "wialon v2.0 // [расширеный пакет]:" do
    test "валидный пакет", sock_info do
      reply = do_send(sock_info, "#D##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;100;NA;NA;;NA;adc:1:1")
      assert reply == "#AD#1\r\n"
    end

    test "не валидная дата/время", sock_info do
      reply = do_send(sock_info, "#D#0;#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;100;NA;NA;;NA;adc:1:1")
      assert reply == "#AD#0\r\n"
    end

    test "не валидные координаты", sock_info do
      reply = do_send(sock_info, "#D##{curr_date()};#{curr_time()};55044.6025;N;03739.6834;E;10;56;100;13;100;NA;NA;;NA;adc:1:1")
      assert reply == "#AD#10\r\n"
    end

    test "не валидная скорость", sock_info do
      reply = do_send(sock_info, "#D##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;-10;56;100;13;100;NA;NA;;NA;adc:1:1")
      assert reply == "#AD#11\r\n"
    end

    test "не валидное количество спутников", sock_info do
      reply = do_send(sock_info, "#D##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;-13;100;NA;NA;;NA;adc:1:1")
      assert reply == "#AD#12\r\n"
    end

    test "не валидная структура (добавлены лишние параметры)", sock_info do
      reply = do_send(sock_info, "#D##{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;758;321")
      assert reply == "#AD#-1\r\n"
    end
  end

  describe "wialon v2.0 // [пакет проверки соединения]:" do
    test "_", %{auth: %{imei: imei, password: password}} = sock_info do
      socket = connect(sock_info)
      body = "2.0;#{imei}#{Enum.random(1..1000000)};#{password};"
      crc = LiveTrack.Crc16.calc(body)
      {:ok, _reply} = send_and_fetch(socket, "#L##{body}#{crc}\r\n")
      {:ok, reply} = send_and_fetch(socket, "#P#\r\n")
      :gen_tcp.close(socket)

      assert reply == "#AP#\r\n"
    end
  end

  describe "wialon v2.0 // [пакеты прошлого периода]:" do
    test "валидные данные", %{auth: %{imei: imei, password: password}} = sock_info do
      socket = connect(sock_info)
      body = "2.0;#{imei}#{Enum.random(1..1000000)};#{password};"
      crc = LiveTrack.Crc16.calc(body)
      {:ok, _reply} = send_and_fetch(socket, "#L##{body}#{crc}\r\n")

      sd_pack = "#{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13"
      d_pack  = "#{curr_date()};#{curr_time()};5544.6025;N;03739.6834;E;10;56;100;13;100;NA;NA;;NA;adc:1:1"
      body = "#{sd_pack}|#{d_pack}|"

      crc = LiveTrack.Crc16.calc(body)
      {:ok, reply} = send_and_fetch(socket, "#B##{body}#{crc}\r\n")
      :gen_tcp.close(socket)

      assert reply == "#AB#2\r\n"
    end
  end

  defp do_send(%{auth: %{imei: imei, password: password}} = sock_info, msg) do
    socket = connect(sock_info)
    body = "2.0;#{imei}#{Enum.random(1..1000000)};#{password};"
    crc = LiveTrack.Crc16.calc(body)
    {:ok, _reply} = send_and_fetch(socket, "#L##{body}#{crc}\r\n")
    body = msg |> String.split("#") |> List.last()
    crc = LiveTrack.Crc16.calc(body)
    {:ok, reply} = send_and_fetch(socket, "#{msg}#{crc}\r\n")
    :gen_tcp.close(socket)

    reply
  end

  defp curr_date() do
    {:ok, date} = Timex.format(:calendar.local_time(), "%d%m%y", :strftime)
    date
  end

  defp curr_time() do
    {:ok, time} = Timex.format(:calendar.local_time(), "%H%M%S", :strftime)
    time
  end

  defp connect(%{host: host, port: port}) do
    {:ok, socket} = :gen_tcp.connect(host, port, @tcp_attrs)
    socket
  end

  defp send_and_fetch(socket, msg, timeout \\ 1000) do
    :ok = send_msg(socket, msg)
    await_msg(socket, timeout)
  end

  defp send_msg(socket, msg), do: :gen_tcp.send(socket, msg)
  defp await_msg(socket, timeout), do: :gen_tcp.recv(socket, 0, timeout)
end
