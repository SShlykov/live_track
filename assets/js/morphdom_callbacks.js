const callbacks = {
  onBeforeElUpdated(from, to){
    if(from.__x){ window.Alpine.clone(from.__x, to) }
    for (const attr of from.attributes) {
      if (attr.name.startsWith("data-js-")) {
        to.setAttribute(attr.name, attr.value);
      }
    }
  },
  onBeforeNodeAdded(node) {
  },
  onNodeAdded(node) {
    if (node?.tagName == "FORM") {
      node.addEventListener('submit', e => e.preventDefault())
    }
    if (node.nodeType === Node.ELEMENT_NODE && node.hasAttribute("autofocus")) {
      node.focus();

      if (node.setSelectionRange && node.value) {
        const lastIndex = node.value.length;
        node.setSelectionRange(lastIndex, lastIndex);
      }
    }
  },
};

export default callbacks;
