const ScrollOnUpdate = {
  mounted() {
    this.__scroll();
  },

  updated() {
    this.__scroll();
  },

  __scroll() {
    this.el.scrollTop = this.el.scrollHeight;
  },
};

export default ScrollOnUpdate;
