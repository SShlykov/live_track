const lowerFirstLetter = (string) => {
  if (string && string[0] && type(string) === "String")
    return string[0].toLowerCase() + string.slice(1);
  else {
    const error = `error by func lowerFirstLetter, ivalid string. string can't equal ${string}`;
    console.error(error);
    return error;
  }
};

export default lowerFirstLetter;
