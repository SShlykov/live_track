import { inc, type, cond, equals, T, pipe } from "ramda";
import { notNilAndNotNan } from "./validators";

export const isDate = (date) =>
  type(date) === "Date" || new Date(date).getTime() > 0;
export const formateDateToString = (date) =>
  isDate(date) && date.toLocaleString();

const monthNames = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];

export const reformateDateString = (date, format = "full") =>
  !!date ? reformate(date, format) : null;

const reformate = (date, format) =>
  cond([
    [
      equals("time-only"),
      () => {
        const { hr, min, sec } = getTime(date);
        return `${hr}:${min}:${sec}`;
      },
    ],
    [
      equals("date-only"),
      () => {
        const { day, mth, year } = getDate(date);
        return `${day}/${mth}/${year}`;
      },
    ],
    [
      equals("date-report"),
      () => {
        const { day, mth, year } = getDate(date);
        return `${year}-${mth}-${day}`;
      },
    ],
    [
      equals("mth-date-report"),
      () => {
        date = new Date(date);
        return `${getMthName(date)} ${date.getDate()}`;
      },
    ],
    [
      T,
      () => {
        const { hr, min, sec } = getTime(date);
        const { day, mth, year } = getDate(date);
        return `${day}/${mth}/${year} ${hr}:${min}:${sec}`;
      },
    ],
  ])(format);

export const addHours = (oldDate, h) => {
  const date = new Date(oldDate);
  date.setHours(date.getHours() + h);
  return date;
};

export const lastDay = addHours(new Date(), -24);

const getTime = (date) => {
  date = new Date(date);
  return {
    hr: formatTime(date.getHours()),
    minutes: formatTime(date.getMinutes()),
    sec: formatTime(date.getSeconds()),
  };
};

const getDate = (date) => {
  date = new Date(date);
  return {
    mth: formatDate(date.getMonth()),
    day: date.getDate(),
    year: date.getFullYear(),
  };
};

export const getMthName = (date) => monthNames[date.getMonth()];
export const formatDate = (t) => (inc(t) >= 10 ? inc(t) : `0${inc(t)}`);
export const formatTime = (t) => `0${t}`.slice(-2);

export const getDateByType = (date) => {
  if (type(date) === "String") return new Date(date.replace(/\s/, "T"));
  return new Date(date);
};

export const parseSepareteDate = (
  date = new Date(),
  symbol = ".",
  mode = "DateDMY"
) => {
  const selfDate = getDateByType(date);
  const dateBits = {
    year: selfDate.getFullYear(),
    month: selfDate.getMonth() + 1,
    day: selfDate.getDate(),
    hours: selfDate.getHours(),
    minutes: selfDate.getMinutes(),
    seconds: selfDate.getSeconds(),
  };
  Object.keys(dateBits).forEach((key) => {
    if (dateBits[key] < 10) dateBits[key] = `0${dateBits[key]}`;
  });
  switch (mode) {
    case "DateDMY":
      return dateBits.day + symbol + dateBits.month + symbol + dateBits.year;
    case "DateDM":
      return dateBits.day + symbol + dateBits.month;
    case "DateYMD":
      return dateBits.year + symbol + dateBits.month + symbol + dateBits.day;
    case "DateDMYHM":
      return `${
        dateBits.day + symbol + dateBits.month + symbol + dateBits.year
      } ${dateBits.hours}:${dateBits.minutes}`;
    case "DateDMYHMS":
      return `${
        dateBits.day + symbol + dateBits.month + symbol + dateBits.year
      } ${dateBits.hours}:${dateBits.minutes}:${dateBits.seconds}`;
    case "DateYMDHM":
      return `${
        dateBits.year + symbol + dateBits.month + symbol + dateBits.day
      } ${dateBits.hours}:${dateBits.minutes}`;
    case "DateYMDHMS":
      return `${
        dateBits.year + symbol + dateBits.month + symbol + dateBits.day
      } ${dateBits.hours}:${dateBits.minutes}:${dateBits.seconds}`;
    case "DateHM":
      return `${dateBits.hours}:${dateBits.minutes}`;
    case "DateHMDM":
      return `${dateBits.hours}:${dateBits.minutes} ${dateBits.day}${symbol}${dateBits.month}`;
    case "DateDataBase":
      return `${
        dateBits.year + symbol + dateBits.month + symbol + dateBits.day
      }T${dateBits.hours}:${dateBits.minutes}:${dateBits.seconds}`;
    case "DateDataBaseSpace":
      return `${
        dateBits.year + symbol + dateBits.month + symbol + dateBits.day
      } ${dateBits.hours}:${dateBits.minutes}:${dateBits.seconds}`;
    default:
      return dateBits.day + symbol + dateBits.month + symbol + dateBits.year;
  }
};

export const toStartDay = (date = new Date()) => {
  const selfDate = getDateByType(date);

  return selfDate.setHours(0, 0, 0);
};

export const withTime = (date, time) =>
  pipe(
    (x) => parseSepareteDate(x, "-", "DateYMD"),
    (x) => `${x}T${time}`
  )(date);
