import { notification } from "antd";
import { pipe, curry, is, cond, T } from "ramda";
import { isArr, isString } from "./validators";

const stringify = curry((a, b, c) => JSON.stringify(c, a, b));
const parse = (str) => JSON.parse(str);

export const wrapNumberMask = (value) => {
  if (is(Number, value)) return value;
  if (value === "" || !is(String, value)) return "";
  let str = "";
  const num = /[0-9]/g;
  if (value) str = value.match(num);
  if (str) {
    str = str.join("");
    return str;
  }
  return "";
};
/**
 * Показывает алерт с информацией
 * @param {string} title title
 * @param {string} description description
 * @param {string} type can be: success info warning error
 */

export const openNotifi = (title, description, type) => {
  notification[type]({
    message: title,
    description,
    placement: "bottomRight",
    className: "zi10000",
  });
};

const getError = (error) =>
  cond([
    [
      () =>
        isArr(error?.response?.errors[0]) &&
        error?.response?.errors[0]?.message,
      () => error?.response?.errors[0]?.message,
    ],
    [() => error.toString],
  ])();

const prepErrText = (error = "") =>
  cond([
    [
      () =>
        isString(error) &&
        error.includes(
          '"response":{"error":"Internal Server Error","status":500'
        ),
      () => "Нет ответа от сервера",
    ],
    [T, () => error],
  ])(error);

/**
 * Выводит gql ошибку
 * @param {string} title title
 * @param {string} description description
 * @param {string} type can be: success info warning error
 */
export const openGqlErrNotifi = (
  error,
  measures = "",
  status = "error",
  title = "Ошибка"
) => {
  const err = pipe(stringify(undefined, 2), parse)(error) || error.toString();

  try {
    let errorText = cond([
      [
        () =>
          isArr(err?.response?.errors) &&
          err?.response?.errors.length > 0 &&
          err?.response?.errors[0]?.message,
        () => {
          let error = err?.response?.errors[0]?.message;
          try {
            //TODO дора,отать
            console.log(JSON.parse(error));
          } catch (error) {
            console.log("openGqlErrNotifi undexpected error");
          }

          return error;
        },
      ],
      [() => error.toString, () => error.toString()],
      [T, () => ""],
    ])();
    errorText = prepErrText(errorText);
    openNotifi(title, `${measures}${errorText}`, status, 20);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error("ошика в модуле utils.openGqlErrNotifi");
    console.log(error);
  }
  return err;
};
