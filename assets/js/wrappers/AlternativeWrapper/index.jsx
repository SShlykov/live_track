import React from "react";
import T from "prop-types";

const AlternativeWrapper = ({ children, isAvaliable, alternative }) => {
  if (!isAvaliable) return <>{alternative}</>;

  return <>{children}</>;
};

AlternativeWrapper.propTypes = {
  alternative: T.oneOfType([T.node, () => null]),
  children: T.oneOfType([T.node, () => null]),
  isAvaliable: T.any,
};

AlternativeWrapper.defaultProps = {
  alternative: null,
  children: null,
  isAvaliable: true,
};

export default AlternativeWrapper;
