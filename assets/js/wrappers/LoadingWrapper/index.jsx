import React from "react";
import T from "prop-types";

const LoadingWrapper = ({ isLoading, children, subTitle, ...restProps }) => {
  if (isLoading) {
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          opacity: 0.6,
        }}
        {...restProps}
      >
        Идет загрузка
      </div>
    );
  }
  return <>{children}</>;
};

LoadingWrapper.propTypes = {
  isLoading: T.bool,
  subTitle: T.oneOfType([() => null, T.string]),
  children: T.node,
};

LoadingWrapper.defaultProps = {
  isLoading: false,
  subTitle: null,
  children: T.node,
};

export default LoadingWrapper;
