import React from "react";
import { Switch } from "antd";
import T from "prop-types";
import { SwitchWrapper } from "./styles";

const ViewSwitcher = ({
  isActive,
  setIsActive,
  className,
  leftTitle = "",
  rightTitle = "",
  disabled = false,
}) => {
  const handleToggle = () => !disabled && setIsActive(!isActive);

  return (
    <div
      style={{
        opacity: disabled ? "0.6" : "1",
      }}
      className={`flex pointer ${className}`}
    >
      <span
        style={{
          color: !isActive && "gray",
          marginRight: 10,
        }}
        onClick={() => {
          !isActive && !disabled && setIsActive(true);
        }}
      >
        {leftTitle}
      </span>
      <SwitchWrapper>
        <Switch checked={!isActive} onClick={handleToggle} />
      </SwitchWrapper>
      <span
        style={{
          color: isActive && "gray",
        }}
        onClick={() => {
          isActive && !disabled && setIsActive(false);
        }}
      >
        {rightTitle}
      </span>
    </div>
  );
};

ViewSwitcher.propTypes = {
  isActive: T.bool,
  setIsActive: T.func,
  className: T.string,
};

ViewSwitcher.defaultProps = {
  isActive: false,
  setIsActive: () => {},
  className: "",
};

export default ViewSwitcher;
