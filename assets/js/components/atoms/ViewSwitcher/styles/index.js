import styled from "styled-components";

export const SwitchWrapper = styled.div`
  & .ant-switch {
    background-color: purple;
  }
  & .ant-switch-checked {
    background-color: #1890ff;
  }
  margin-right: 10px !important;
`;
