import styled from "styled-components";

export const Wrapper = styled.button`
  border: 1px solid #e1e1e1;
  z-index: 3;
  cursor: pointer;
  position: relative;
  border-radius: 3px;
  height: 40px;
  width: 40px;
  align-items: center;
  display: flex;
  justify-content: center;
  background-color: white;
  transition: all 0.3s;
  color: #cccccc;
  font-size: 20px;
  &:hover {
    border: 1px solid #cccccc;
  }
  @media screen and (max-width: 500px) {
    height: 30px;
    width: 30px;
    & * {
      transform: scale(0.9);
    }
  }
`;
