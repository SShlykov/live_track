import React from "react";
import { Wrapper } from "./styles";
import CountIcon from "../CountIcon";
import { is } from "ramda";
import { randKey } from "../../../../helpers/frontend_utils/index";

const CounIcons = ({ legend, ...restProps }) => {
  if (!is(Array, legend)) return null;
  return (
    <Wrapper {...restProps} className="count-icons">
      {legend &&
        legend?.map((props) => (
          <CountIcon className="count-icon" key={randKey()} {...props} />
        ))}
    </Wrapper>
  );
};

export default CounIcons;
