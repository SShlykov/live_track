import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 20px;
  background-color: white;
  @media screen and (max-width: 550px) {
    width: 100%;
    flex-direction: row;
    justify-content: center;
  }
`;
