import React from "react";
import { Wrapper, Icon } from "./styles";
import { Train } from "../icons";

export default ({ count = 0, color = "", title = "", ...restProps }) => {
  return (
    <Wrapper color={color} {...restProps}>
      <Icon color={color}>
        <Train
          size={12}
          styles={{
            color,
          }}
        />
      </Icon>
      <div className="title">{title}</div>
      <div className="count">{count}</div>
    </Wrapper>
  );
};
