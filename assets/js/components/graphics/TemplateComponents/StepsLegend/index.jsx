import { Wrapper } from "./styles";

const StepsLegend = ({ railways, orgs }) => {
  const railwayName = railways?.name || null;
  const orgsName = orgs?.name || null;

  const hasPath = railwayName || orgsName;

  const rawPath = [railwayName, orgsName].filter((el) => el);

  const path = rawPath.map(
    (el, idx) =>
      el && (
        <div className="legend">
          {el}
          {idx + 1 < rawPath.length ? "," : ""}
        </div>
      )
  );

  if (!railwayName) return null;

  return <Wrapper>Выбрано: {path}</Wrapper>;
};
export default StepsLegend;
