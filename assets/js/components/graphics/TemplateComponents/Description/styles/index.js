import styled from "styled-components";
import { darken } from "polished";

export const Wrapper = styled.p`
  display: flex;
  color: gray;
  align-items: center;
  & svg {
    height: 15px;
  }
`;

export const BreadBit = styled.span`
  cursor: pointer;
  transition: all 0.3s;
  text-decoration: underline dotted;
  &:hover {
    text-decoration: underline;
    color: ${darken(0.3, "gray")};
  }
`;
