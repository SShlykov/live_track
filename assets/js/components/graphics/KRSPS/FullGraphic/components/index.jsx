import React from "react";
import { cond, T, equals } from "ramda";
import FullPie from "./FullPie";
import { Spin } from "antd";

export default ({ graphType, loading, ...props }) => {
  if (loading) return <Spin />;
  if (!props.data) return <Spin />;

  return cond([
    [equals("FullPie"), () => <FullPie {...props} />],
    [T, () => null],
  ])(graphType);
};
