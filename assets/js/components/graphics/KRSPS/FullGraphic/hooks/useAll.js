import { useState } from "react";

export default ({
  setData,
  setLegend,
  dtStart,
  dtFinish,
  machinesidx,
  retries,
  serverProps,
}) => {
  const [loadingSummary, setLoadingSummary] = useState(false);
  const [error, setError] = useState(null);

  const getDataSummary = () => {
    setLoadingSummary(true);

    serverProps.pushEvent("get_conn_data", {
      dates: { start: dtStart, finish: dtFinish },
      group_by: { classifications: [140], railways: [], orgs: [] },
      available_machines: machinesidx,
    });

    // machinesidx &&
    //   request({
    //     schema: "ASKR_ON_OFF_REPORT_ALL",
    //     token: localStorage.getItem("auth-token"),
    //     data: {
    //       start: dtStart,
    //       finish: dtFinish,
    //       machines: machinesidx,
    //     },
    //   })
    //     .then((data) => {
    //       if (data?.getConnectionsReportDiagramData[0]) {
    //         setData(data?.getConnectionsReportDiagramData);
    //         const { hadConnection, hadNotConnection } =
    //           data?.getConnectionsReportDiagramData[0];
    //         setLegend([
    //           {
    //             color: "darkgreen",
    //             count: hadConnection,
    //             title: "Выходили на связь",
    //           },
    //           {
    //             color: "darkred",
    //             count: hadNotConnection,
    //             title: "Не выходили на связь",
    //           },
    //         ]);
    //       }
    //       setLoadingSummary(false);
    //     })
    //     .catch((e) => {
    //       console.log(e);

    //       setTimeout(() => {
    //         retries < limit && setRetries(retries + 1);
    //       }, 500);

    //       setError(e);
    //       setLoadingSummary(false);
    //     });

    setTimeout(() => {
      setLoadingSummary(false);
    }, 6000);
  };

  return { getDataSummary, loadingSummary, error };
};
