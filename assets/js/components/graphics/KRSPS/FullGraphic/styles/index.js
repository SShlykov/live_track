import styled from "styled-components";
import { cond, T, and } from "ramda";

// fullScreeEnabled && !isFullScreen ? "display: flex;" : "display: none;"

export const Wrapper = styled.div`
  ${({ fullScreeEnabled, isFullScreen }) =>
    cond([
      [and(!isFullScreen), () => "display: none;"],
      [T, () => "display: flex;"],
    ])(fullScreeEnabled)}
  flex-direction: column;
  margin-right: 0;
  align-items: center;
  background-color: white;
  padding: 20px 20px 20px 0;
  position: relative;
  overflow-x: hidden;
  overflow-y: hidden;
  height: ${({ isFullScreen }) => (isFullScreen ? "100%" : "410px")};
  background-color: white;

  flex: 1 1 520px;

  @media screen and (max-width: 550px) {
    width: 100%;
    height: 560px;
    & .fullscreen {
      display: none;
    }
  }
`;

export const Title = styled.div`
  display: flex;
  color: black;
  column-gap: 20px;
  width: 100%;
  padding-left: 20px;
  justify-content: space-between;
  align-items: center;
  position: relative;
  & * h2 {
    font-size: 16px;
    font-weight: 600;
    color: #242320;
    @media screen and (max-width: 550px) {
      font-size: 14px;
      max-width: 250px;
    }
  }
  & * p {
    font-size: 11px;
    color: gray;
  }
`;

export const TitleContent = styled.div`
  display: flex;
  align-items: center;
  column-gap: 20px;
`;

export const Body = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  overflow: hidden;
  background-color: white;
  & .count-icons {
    transform: scale(0.9);
    row-gap: 10px;
    & .count-icon {
      transform: scale(0.9);
    }
  }
  @media screen and (max-width: 550px) {
    width: 100%;
    flex-direction: column;
    justify-content: center;
  }
`;

export const GrapMesures = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  column-gap: 15px;
`;
