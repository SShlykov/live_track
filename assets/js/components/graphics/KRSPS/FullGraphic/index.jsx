/* eslint-disable import/no-anonymous-default-export */
import React, { useState, useEffect } from "react";
import { Wrapper, Title, Body, TitleContent, GrapMesures } from "./styles";
import {
  CounIcons,
  DownloadButton,
  Description,
  FullScreenButton,
} from "../../TemplateComponents";
import SmartGraphic from "./components";
import { useAll } from "./hooks";
import { getDescription } from "../../funcs";
import { AlternativeWrapper, LoadingWrapper } from "../../../../wrappers";

const FullGraphic = (props) => {
  const [rendered, setRendered] = useState(false);

  const {
    dtStart: start,
    dtFinish: finish,
    machinesidx: machIdx,
    containerId,
    fullScreeEnabled,
  } = props;

  const [legend, setLegend] = useState([]);
  const [data, setData] = useState(null);
  const [menuOpened, setMenuOpened] = useState(false);
  const [machinesidx] = useState(machIdx);
  const [dtStart] = useState(start);
  const [dtFinish] = useState(finish);
  const [retries, setRetries] = useState(0);
  const limit = 3;

  const serverProps = {
    handleEvent: props?.handleEvent || null,
    pushEvent: props?.pushEvent || null,
  };

  if (props.handleEvent) {
    props.handleEvent("fetch_conn_data", ({ data }) => {
      setData(data);
    });
  }

  const { getDataSummary, loadingSummary } = useAll({
    setData,
    setLegend,
    dtStart,
    dtFinish,
    machinesidx,
    retries,
    setRetries,
    limit,
    serverProps,
  });

  const loading = loadingSummary;

  useEffect(() => {
    setTimeout(() => {
      setRendered(true);
    }, 3000);
  }, []);

  params = {
    dates: { start: dtStart, finish: dtFinish },
    group_by: { classifications: [140], railways: [76], orgs: [] },
    available_machines: machinesidx,
  };

  useEffect(() => {
    serverProps?.pushEvent && serverProps?.pushEvent("init_component", params);
  }, [serverProps?.pushEvent]);

  const handleClick = () => {};

  const total =
    data &&
    data.reduce(
      (acc, { hadConnection, hadNotConnection }) =>
        acc + hadConnection + hadNotConnection,
      0
    );

  const button = rendered && (
    <DownloadButton
      containerId={containerId}
      setMenuOpened={setMenuOpened}
      menuOpened={menuOpened}
      width={650}
      {...props}
    />
  );

  return (
    <AlternativeWrapper
      alternative={null}
      isAvaliable={loading || (data && data[0])}
    >
      <Wrapper
        fullScreeEnabled={fullScreeEnabled}
        isFullScreen={props?.isFullScreen}
        id={containerId}
      >
        <LoadingWrapper subTitle="данные загружаются" isLoading={loading}>
          <Title>
            <TitleContent>
              <div>
                <h2>Количество КРСПС, выходивших на связь (всего {total})</h2>
                <Description dtStart={dtStart} dtFinish={dtFinish} />
              </div>
            </TitleContent>
            <GrapMesures>
              {button}
              <FullScreenButton
                className={"fullscreen"}
                graphicIdx={1}
                {...props}
              />
            </GrapMesures>
          </Title>
          <Body>
            <div
              style={{
                flexGrow: 1,
                display: "flex",
                justifyContent: "center",
                overflow: "hidden",
                backgroundColor: "white",
              }}
            >
              <SmartGraphic
                loading={loading}
                data={data}
                graphType={"FullPie"}
                changeStep={handleClick}
                subtitle={getDescription({ dtStart, dtFinish })}
                total={total}
                {...props}
              />
            </div>
            <CounIcons
              style={{
                marginBottom: 40,
              }}
              legend={legend}
            />
          </Body>
        </LoadingWrapper>
      </Wrapper>
    </AlternativeWrapper>
  );
};

export default FullGraphic;
