import { useState } from "react";

export default ({ setData }) => {
  const [loadingByDrp, setLoadingByDrp] = useState(false);

  const [error, setError] = useState(null);

  const getDataByDrp = (data) => {
    setLoadingByDrp(true);

    // request({
    //   schema: "GET_ON_OFF_AGREGATED_DRP_REPORT",
    //   token: localStorage.getItem("auth-token"),
    //   data,
    // })
    //   .then((data) => {
    //     data?.getConnectionsReportDiagramData &&
    //       setData(data.getConnectionsReportDiagramData);
    //     setLoadingByDrp(false);
    //   })
    //   .catch((e) => {
    //     console.log(e);
    //     setError(e);
    //     setLoadingByDrp(false);
    //   });
  };

  return { getDataByDrp, loadingByDrp, error };
};
