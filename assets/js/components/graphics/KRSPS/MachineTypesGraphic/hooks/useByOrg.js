import { useState } from "react";

export default ({ setData }) => {
  const [loadingByOrgs, setLoadingByOrgs] = useState(false);

  const [error, setError] = useState(null);

  const getDataByOrgs = (data) => {
    setLoadingByOrgs(true);
    // request({
    //   schema: "GET_ON_OFF_AGREGATED_ORG_REPORT",
    //   token: localStorage.getItem("auth-token"),
    //   data,
    // })
    //   .then((data) => {
    //     data?.getConnectionsReportDiagramData &&
    //       setData(data.getConnectionsReportDiagramData);
    //     setLoadingByOrgs(false);
    //   })
    //   .catch((e) => {
    //     console.log(e);
    //     setError(e);
    //     setLoadingByOrgs(false);
    //   });
  };
  return { getDataByOrgs, loadingByOrgs, error };
};
