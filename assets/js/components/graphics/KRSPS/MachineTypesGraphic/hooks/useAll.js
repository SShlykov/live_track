import { useMutation } from "@apollo/react-hooks";
import { ASKR_ON_OFF_REPORT_ALL } from "../gql";

export default ({ setData, setLegend, dtStart, dtFinish }) => {
  const [getDataSummary, { loadingSummary, error }] = useMutation(
    ASKR_ON_OFF_REPORT_ALL,
    {
      variables: {
        start: dtStart,
        finish: dtFinish,
      },
      onError: (e) => console.log(e),
      onCompleted: (data) => {
        if (data?.getConnectionsReportDiagramData[0]) {
          setData(data?.getConnectionsReportDiagramData);
          const { hadConnection, hadNotConnection } =
            data?.getConnectionsReportDiagramData[0];
          setLegend([
            {
              color: "darkgreen",
              count: hadConnection,
              title: "Выходили на связь",
            },
            {
              color: "darkred",
              count: hadNotConnection,
              title: "Не выходили на связь",
            },
          ]);
        }
      },
    }
  );
  return { getDataSummary, loadingSummary, error };
};
