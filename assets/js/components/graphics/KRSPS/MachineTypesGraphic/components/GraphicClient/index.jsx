/* eslint-disable import/no-anonymous-default-export */
import React, { useLayoutEffect, useState } from "react";
import {
  Wrapper,
  Body,
  ControllBlock,
  TitleDesktop,
  TitleContent,
  TitleMobile,
} from "./styles";

import {
  ErrorBoundry,
  DownloadButton,
  BackButton,
  Description,
  FullScreenButton,
  MobileBreadButtons,
} from "../../../../TemplateComponents";
import SmartGraphic from "../../components";
import AlternativeWrapper from "../../../../../../wrappers/AlternativeWrapper";
import LoadingWrapper from "../../../../../../wrappers/LoadingWrapper";

const GraphicClient = (props) => {
  const [rendered, setRendered] = useState(false);

  const {
    total,
    setMenuOpened,
    menuOpened,
    handleClick,
    step,
    loading,
    containerId,
    data,
  } = props;

  useLayoutEffect(() => {
    setTimeout(() => {
      setRendered(true);
    }, 3000);
  }, []);

  const donwloadBtn = (props) =>
    rendered && (
      <DownloadButton
        containerId={containerId}
        setMenuOpened={setMenuOpened}
        menuOpened={menuOpened}
        height={900}
        {...props}
      />
    );

  return (
    <AlternativeWrapper alternative={null} isAvaliable={data && data[0]}>
      <Wrapper
        fullScreeEnabled={props?.fullScreeEnabled}
        isFullScreen={props?.isFullScreen}
      >
        <LoadingWrapper subTitle="данные загружаются" isLoading={loading}>
          <TitleDesktop>
            <TitleContent>
              <div>
                <h2>Количество КРСПС, выходивших на связь (всего {total})</h2>
                <Description {...props} />
              </div>
            </TitleContent>
            <ControllBlock>
              {donwloadBtn()}
              <FullScreenButton
                className="fullscreen"
                graphicIdx={1}
                {...props}
              />
              {step > 0 && <BackButton onClick={() => handleClick(step - 1)} />}
            </ControllBlock>
          </TitleDesktop>
          <TitleMobile>
            <TitleContent>
              <div
                style={{
                  display: "flex",
                }}
              >
                <h2>Количество КРСПС, выходивших на связь (всего {total})</h2>
                <Description {...props} pathList={null} />
              </div>
            </TitleContent>
            <div
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "space-between",
              }}
            >
              <MobileBreadButtons {...props} />
              <ControllBlock>
                {donwloadBtn()}
                <FullScreenButton
                  className="fullscreen"
                  graphicIdx={1}
                  {...props}
                />
                {step > 0 && (
                  <BackButton onClick={() => handleClick(step - 1)} />
                )}
              </ControllBlock>
            </div>
          </TitleMobile>
          <Body>
            <ErrorBoundry>
              <SmartGraphic {...props} />
            </ErrorBoundry>
          </Body>
        </LoadingWrapper>
      </Wrapper>
    </AlternativeWrapper>
  );
};

export default GraphicClient;
