/* eslint-disable import/no-anonymous-default-export */
import React, { useLayoutEffect, useState } from "react";
import {
  Wrapper,
  Body,
  ControllBlock,
  TitleDesktop,
  TitleContent,
  TitleMobile,
} from "./styles";

import {
  ErrorBoundry,
  DownloadButton,
  Description,
} from "../../../../TemplateComponents";
import SmartGraphic from "../../components";
import LoadingWrapper from "../../../../../../wrappers/LoadingWrapper";

const GraphicClient = (props) => {
  const { total, loading, containerId, data } = props;

  if (!data || !data[0]) return null;

  return (
    <Wrapper
      fullScreeEnabled={props?.fullScreeEnabled}
      isFullScreen={props?.isFullScreen}
      id={containerId}
    >
      <LoadingWrapper subTitle="данные загружаются" isLoading={loading}>
        <TitleDesktop>
          <TitleContent>
            <div>
              <h2>Количество КРСПС, выходивших на связь (всего {total})</h2>
              <Description {...props} />
            </div>
          </TitleContent>
        </TitleDesktop>
        <Body>
          <ErrorBoundry>
            <SmartGraphic downloadBar={true} {...props} />
          </ErrorBoundry>
        </Body>
      </LoadingWrapper>
    </Wrapper>
  );
};

export default GraphicClient;
