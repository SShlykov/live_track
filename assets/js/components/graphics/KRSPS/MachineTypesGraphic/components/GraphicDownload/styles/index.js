import styled from "styled-components";
import { cond, T, and } from "ramda";

export const Wrapper = styled.div`
  ${({ fullScreeEnabled, isFullScreen }) =>
    cond([
      [and(!isFullScreen), () => "display: none;"],
      [T, () => "display: flex;"],
    ])(fullScreeEnabled)}
  position: relative;
  flex-direction: column;
  margin-right: 0;
  align-items: flex-start;
  background-color: white;
  padding: 10px 0px 0px 0;

  width: 520px;
  height: 900px;
`;

export const TitleDesktop = styled.div`
  display: flex;
  position: absolute;
  z-index: 3;
  background-color: white;
  color: black;
  column-gap: 20px;
  width: 95%;
  padding: 20px 0 0 20px;
  justify-content: space-between;
  align-items: flex-end;
  & * h2 {
    font-size: 14px;
    font-weight: 600;
    color: #242320;
  }
  & * p {
    font-size: 9px;
    color: gray;
  }
`;

export const TitleMobile = styled.div`
  display: none;
  position: absolute;
  z-index: 3;
  background-color: white;
  flex-direction: column;
  color: black;
  column-gap: 20px;
  row-gap: 5px;
  width: 95%;
  padding: 20px 0 0 20px;
  justify-content: center;
  align-items: center;
  & * h2 {
    font-size: 12px;
    max-width: 170px;
    color: #242320;
  }
  & * p {
    font-size: 9px;
    color: gray;
    max-width: 170px;
    height: 28px;
  }
`;

export const TitleContent = styled.div`
  display: flex;
  align-items: center;
  column-gap: 20px;
`;

export const Body = styled.div`
  width: 100%;
  flex-grow: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ControllBlock = styled.div`
  display: flex;
  top: 30px;
  column-gap: 10px;
  right: 50px;
  z-index: 2;
`;
