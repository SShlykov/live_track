import BarGraphic from "../BarGraphic";
import { T, includes, cond } from "ramda";

export default ({ data = [], ...restProps }) => {
  const type = data[0]?.sps || "";

  const left = cond([
    [includes("УК-25/25"), () => 100],
    [includes("25/28СП"), () => 110],
    [includes("ПО-3-3000"), () => 110],
    [T, () => 80],
  ])(type);

  return (
    <BarGraphic
      {...restProps}
      left={left}
      data={data.map(({ machines, sps, ...props }) => ({
        machine: `${sps || ""}`,
        sps,
        ...props,
      }))}
    />
  );
};
