import BarGraphicClient from "../BarGraphicClient";
import BarGraphicDownload from "../BarGraphicDownload";

export default (props) => {
  if (props?.downloadBar) return <BarGraphicDownload {...props} />;

  return <BarGraphicClient {...props} />;
};
