import { cond, equals, T } from "ramda";

export const getDescription = ({ dtStart, dtFinish, path = "" }) => {
  const days =
    (new Date(dtFinish).getTime() - new Date(dtStart).getTime()) /
      1000 /
      60 /
      60 /
      24 +
    1;

  const dateText = cond([
    [equals(1), () => "За последние технические сутки"],
    [equals(7), () => "За последнюю неделю"],
    [equals(30), () => "За последний месяц"],
    [T, () => null],
  ])(days);

  const text = `${dateText}${path ? `, ${path}` : ""}`;

  return text;
};

export const pathText = ({ machineType, railways, orgs }) => {
  const railwayName = railways?.name || null;
  const orgsName = orgs?.name || null;
  const machineTypeName = machineType?.name || null;

  const rawPath = [machineTypeName, railwayName, orgsName].filter((el) => el);

  if (!machineTypeName) return "";

  return `выбрано: ${rawPath.join(", ")}`;
};

export const pathList = ({
  machineType,
  railways,
  orgs,
  getTypesData,
  getDrpData,
  getOrgsData,
}) => {
  const indexName = machineType?.name ? "Главная" : null;

  const railwayName = railways?.name || null;
  const orgsName = orgs?.name || null;
  const machineTypeName = machineType?.name || null;

  const rawPath = [
    {
      name: indexName,
      getData: getTypesData,
    },
    {
      name: machineTypeName,
      getData: getDrpData,
    },
    {
      name: railwayName,
      getData: getOrgsData,
    },
    {
      name: orgsName,
      getData: () => null,
    },
  ]
    .filter((el) => el?.name)
    .map((el, idx, arr) => {
      if (arr.length === idx + 1)
        return {
          name: el.name,
          getData: () => null,
        };
      return el;
    });

  if (!machineTypeName) return "";

  return rawPath.map((el, idx) => ({
    ...el,
    name: rawPath?.length - 1 > idx ? `${el?.name} / ` : el?.name,
  }));
};
