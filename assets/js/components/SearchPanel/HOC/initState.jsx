import React from "react";
import { useFilterFields } from "../hooks";

const initState = (WrappedComponent) => {
  const ModifiedComponent = (
    ownProps // модифицированная версия компонента
  ) => {
    const filterProps = useFilterFields({
      available_machines: ownProps?.available_machines || [],
    });

    return <WrappedComponent {...ownProps} {...filterProps} />;
  };

  return ModifiedComponent;
};

export default initState;
