import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  row-gap: 20px;
  column-gap: 20px;
  flex-wrap: wrap;
`;

export const ContainerShadow = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  row-gap: 20px;
  margin-right: 0;
  align-items: center;
  background-color: white;
  justify-content: center;
  padding: 20px;
  box-shadow: 4px 4px 8px 0px rgba(34, 60, 80, 0.2);
  column-gap: 20px;
  flex: 1 1 240px;
`;

export const LeftPanel = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  row-gap: 20px;
  margin-right: 0;
  align-items: center;
  background-color: white;
  padding: 20px;
  box-shadow: 4px 4px 8px 0px rgba(34, 60, 80, 0.2);
  column-gap: 20px;
  width: calc(100% - 260px);
  justify-content: space-between;
  flex-basis: auto;
  flex: 10 1 640px;
  & .my-select {
    flex: 5 1 5%;
    @media only screen and (max-width: 1600px) {
      flex: 2 1 30%;
    }
  }
  & .button {
    flex: 1 1 60px;
  }
  & .select {
    display: flex;
    flex: 1 1 60px;
  }
`;
