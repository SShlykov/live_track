# Test Server

Требования проекта:
* elixir > v1.12
* js > v14
* в общей сети должны быть доступен авторазционный сервис

## Для запуска сервера необходимо стянуть зависимости для phoenix и реакт -

```sh
 mix deps get && cd assets && yarn && cd ..
```

(Опционально) Можно проверить компилируется ли данный проект

```sh
mix compile
```

### Перед запуском проекта необходимо его зарегистрировать его в live monitor (создать сайт) и вписать его имя -

```
config.exs
```

### Для запуска проекта необходимо передать имя, адрес машины и сетевой пароль (пароль должен соответствовать live monitor)

```sh
iex --cookie {net_passwd} --name {net_name@net_add} -S mix phx.server

например:
iex --cookie 8FWrDX6nB9zCreE5 --name test_project@172.25.78.153 -S mix phx.server
```

## Для переноса на прод сервера необходимо сконфигурировать проект -

* исправить адреса в папке rel для той платформы, на которой будут запускаться исходники (bat - windows, sh/bash - unix)
* исправить path и сетевой пароль в функции releases в mix.exs
* .service файл, переименовать и прописать пути к релизу проекта

```sh
cp {project_name}.service /etc/systemd/system/
```

### На сервере (если установлен elixir переносится проект целиком)

```sh
MIX_ENV=prod mix release && mix assets.deploy && systemctl restart {project_name}.service
```

### На сервере (если не установлен elixir, переносится папка с релизом, priv)

```sh
systemctl restart {project_name}.service
```

### (опционально) настройка CI\CD gitlab_ci
* дописать необходимые стадии в папке ci и описать .gitlab-ci.yml
