defmodule Server.Emailer do
  @moduledoc """
  Модуль общения с сервисом вторичной обработки данных
  """
  use Server.CallerFuncs

  use Munin,
    adapter: Munin.FileWritter,
    otp_app: :server,
    log_path: "priv/unhandled_logs",
    service_name: "emailer"

  def important_list, do: ~w()a
  def test_mail(message), do: call({:test_mail, message})
  def test_mail(message, to), do: call({:test_mail, message, to})

  def send_incident(machine, datetime, message_list, users),
    do: call({:send_incident, machine, datetime, message_list, users})

  def send_test_incident(email), do: call({:send_test_incident, email})
  def do_mail(from, send_to, msg), do: call({:do_mail, from, send_to, msg})
end
