defmodule Server.SqlSup do
  @moduledoc """
  Модуль общения с сервисом работы с SQL запросами
  """
  use Server.CallerFuncs

  use Munin,
    adapter: Munin.FileWritter,
    otp_app: :server,
    log_path: "priv/unhandled_logs",
    service_name: "sql_sup"

  def important_list, do: ~w(insert_chunked)a

  def insert_chunked(repo_params, list, table_name, chunk),
    do: call({:insert_chunked, repo_params, list, table_name, chunk})

  def insert_chunked(repo_params, list, table_name),
    do: call({:insert_chunked, repo_params, list, table_name})

  def select_logs(db_auth, table, dt_start, dt_end),
    do: call({:select_logs, db_auth, table, dt_start, dt_end})

  def select_logs_122(auth, table, imei, pack_number, dt_start, dt_finish),
    do: call({:select_logs_122, auth, table, imei, pack_number, dt_start, dt_finish})

  def is_table_exist?(repo_params, table_name),
    do: call({:is_table_exist?, repo_params, table_name})

  def delete_raw_data(repo_params, table_name, params),
    do: call({:delete_raw_data, repo_params, table_name, params})

  def delete_job_data(repo_params, table_name, params),
    do: call({:delete_job_data, repo_params, table_name, params})

  def run_query(repo_params, callback), do: call({:run_query, repo_params, callback})

  def run_query_safely(repo_params, callback),
    do: call({:run_query_safely, repo_params, callback})
end
