defmodule Server.LiveMonitor do
  @moduledoc """
  Модуль общения с сервисом вторичной обработки данных
  """
  use Server.CallerFuncs

  use Munin,
    adapter: Munin.FileWritter,
    otp_app: :server,
    log_path: "priv/unhandled_logs",
    service_name: "live_monitor"

  def important_list, do: ~w()a
  def get_handled_packs(attrs), do: call({:get_handled_packs, attrs})
end
