defmodule Server.Niflheim do
  @moduledoc """
  Модуль общения с сервисом отображения данных
  """
  use Server.CallerFuncs

  use Munin,
    adapter: Munin.FileWritter,
    otp_app: :server,
    log_path: "priv/unhandled_logs",
    service_name: "niflheim"

  def important_list, do: ~w(factorial)a

  def handle_packs(auth, data), do: call({:handle, auth, data})

  def handle_packs(auth, data, for_merge) when is_map(for_merge),
    do: call({:handle, auth, data, for_merge})

  def handle_and_receive(auth, data), do: call({:handle_test, auth, data})

  def handle_and_receive(auth, data, for_merge) when is_map(for_merge),
    do: call({:handle_test, auth, data, for_merge})

  def list_package_managers, do: call(:list_package_managers)
  def get_package_manager(number: number), do: call({:get_package_manager, [{:number, number}]})

  def create_or_update_package_manager(attrs),
    do: call({:create_or_update_package_manager, attrs})

  def delete_package_manager(id), do: call({:delete_package_manager, id})

  def list_users(site_name), do: call({:list_users, site_name})
  def get_user(site_name, id), do: call({:get_user, site_name, id})
  def get_user_by_login(site_name, login), do: call({:get_user_by_login, site_name, login})
  def get_user_role(site_name, id), do: call({:get_user_role, site_name, id})

  def get_user_by_login_and_password(site_name, login, password),
    do: call({:get_user_by_login_and_password, site_name, login, password})

  def register_user(site_name, attrs), do: call({:register_user, site_name, attrs})
  def set_permissions(user, permissions_ids), do: call({:set_permissions, user, permissions_ids})
  def change_user_registration(user, attrs), do: call({:change_user_registration, user, attrs})
  def change_user_login(user, attrs), do: call({:change_user_login, user, attrs})

  def apply_user_login(user, password, attrs),
    do: call({:apply_user_login, user, password, attrs})

  def update_user_login(user, token), do: call({:update_user_login, user, token})

  def deliver_update_login_instructions(user, current_login, update_login_url_fun),
    do: call({:deliver_update_login_instructions, user, current_login, update_login_url_fun})

  def change_user_password(user, attrs), do: call({:change_user_password, user, attrs})

  def update_user_password(user, password, attrs),
    do: call({:update_user_password, user, password, attrs})

  def generate_user_session_token(user), do: call({:generate_user_session_token, user})
  def get_user_by_session_token(token), do: call({:get_user_by_session_token, token})
  def delete_session_token(token), do: call({:delete_session_token, token})

  def deliver_user_confirmation_instructions(user, confirmation_url_fun),
    do: call({:deliver_user_confirmation_instructions, user, confirmation_url_fun})

  def confirm_user(token), do: call({:confirm_user, token})

  def deliver_user_reset_password_instructions(user, reset_password_url_fun),
    do: call({:deliver_user_reset_password_instructions, user, reset_password_url_fun})

  def get_user_by_reset_password_token(token),
    do: call({:get_user_by_reset_password_token, token})

  def reset_user_password(user, attrs), do: call({:reset_user_password, user, attrs})
  def remove_user_forever(attrs), do: call({:remove_user_forever, attrs})

  def list_admins(), do: call(:list_admins)
  def get_admin(id), do: call({:get_admin, id})
  def get_admin_by_login(login), do: call({:get_admin_by_login, login})
  def get_admin_role(id), do: call({:get_admin_role, id})

  def get_admin_by_login_and_password(login, password),
    do: call({:get_admin_by_login_and_password, login, password})

  def register_admin(attrs), do: call({:register_admin, attrs})
  def change_admin_registration(user, attrs), do: call({:change_admin_registration, user, attrs})
  def change_admin_login(user, attrs), do: call({:change_admin_login, user, attrs})

  def apply_admin_login(user, password, attrs),
    do: call({:apply_admin_login, user, password, attrs})

  def update_admin_login(user, token), do: call({:update_admin_login, user, token})

  def deliver_update_admin_login_instructions(user, current_login, update_login_url_fun),
    do:
      call({:deliver_update_admin_login_instructions_, user, current_login, update_login_url_fun})

  def change_admin_password(user, attrs), do: call({:change_admin_password, user, attrs})

  def update_admin_password(user, password, attrs),
    do: call({:update_admin_password, user, password, attrs})

  def generate_admin_session_token(user), do: call({:generate_admin_session_token, user})
  def get_admin_by_session_token(token), do: call({:get_admin_by_session_token, token})
  def delete_admin_session_token(token), do: call({:delete_admin_session_token, token})

  def deliver_admin_confirmation_instructions(user, confirmation_url_fun),
    do: call({:deliver_admin_confirmation_instructions, user, confirmation_url_fun})

  def confirm_admin(token), do: call({:confirm_admin, token})

  def deliver_admin_reset_password_instructions(user, reset_password_url_fun),
    do: call({:deliver_admin_reset_password_instructions, user, reset_password_url_fun})

  def get_admin_by_reset_password_token(token),
    do: call({:get_admin_by_reset_password_token, token})

  def reset_admin_password(user, attrs), do: call({:reset_admin_password, user, attrs})

  @doc """
      iex> LiveMonitor.Niflheim.remove_admin_forever(%{id: 1})
      {:ok, %DbAuth.Accounts.Admin{}}
  """
  def remove_admin_forever(attrs), do: call({:remove_admin_forever, attrs})
  def update_admin(user, attrs), do: call({:update_admin, user, attrs})

  def list_admin_groups, do: call(:list_admin_groups)
  def get_admin_group(id), do: call({:get_admin_group, id})
  def create_admin_group(attrs), do: call({:create_admin_group, attrs})
  def update_admin_group(admin_group, attrs), do: call({:update_admin_group, admin_group, attrs})
  def delete_admin_group(admin_group), do: call({:delete_admin_group, admin_group})

  def list_user_sessions(), do: call(:list_user_sessions)
  def list_user_sessions(uid), do: call({:list_user_sessions, uid})
  def list_active_user_sessions(), do: call(:list_active_user_sessions)
  def list_active_user_sessions(uid), do: call({:list_active_user_sessions, uid})
  def get_user_session(id), do: call({:get_user_session, id})
  def create_user_session(attrs), do: call({:create_user_session, attrs})
  def update_user_session(prev, params), do: call({:update_user_session, prev, params})
  def delete_user_session(id), do: call({:delete_user_session, id})

  def create_user_device_ip(attrs), do: call({:create_user_device_ip, attrs})
  def get_user_device_ip(id), do: call({:get_user_device_ip, id})
  def list_user_device_ips(user_ip), do: call({:list_user_device_ips, user_ip})

  def list_user_device_ips(user_ip, site_name),
    do: call({:list_user_device_ips, user_ip, site_name})

  def update_user_device_ip(prev, params), do: call({:update_user_device_ip, prev, params})
  def delete_user_device_ip(id), do: call({:delete_user_device_ip, id})

  def list_user_session_statistics(session_id),
    do: call({:list_user_session_statistics, session_id})

  def list_user_active_session_statistics(session_id),
    do: call({:list_user_active_session_statistics, session_id})

  def get_prev_user_session_statistic(session_id),
    do: call({:get_prev_user_session_statistic, session_id})

  def get_unfinished_user_session_statistic(session_id),
    do: call({:get_unfinished_user_session_statistic, session_id})

  def get_user_session_statistic(id), do: call({:get_user_session_statistic, id})
  def create_user_session_statistic(attrs), do: call({:create_user_session_statistic, attrs})

  def update_user_session_statistic(prev, params),
    do: call({:update_user_session_statistic, prev, params})

  def delete_user_session_statistic(id), do: call({:delete_user_session_statistic, id})
end
